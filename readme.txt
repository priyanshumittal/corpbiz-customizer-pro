Corpbiz.

A Premium multi colored Business Blog theme that supports Primary menu's , Primary sidebar,Four widgets area at the footer region  etc. 
It has a perfect design that's great for any Business/Firms  Blogs who wants a new look for their site. Three page templates Home ,Blog and Contact Page. 
Theme supports featured slider managed from Theme Option Panel.

Author: Priyanshu Mittal,Hari Maliya,Shahid Mansuri and Vibhor Purandare,harish.
Theme Homepage Url:http://webriti.com/demo/wp/corpbiz/

About:
Corpbiz-Pro a theme for business, consultancy firms etc  by Webriti (Author URI: http://www.webriti.com). 

The CSS, XHTML and design is released under GPL:
http://www.opensource.org/licenses/gpl-license.php

Feel free to use as you please. I would be very pleased if you could keep the Auther-link in the footer. Thanks and enjoy.

Appoinment supports Custom Menu, Widgets and 
the following extra features:

 - Pre-installed menu and content colors
 - Responsive
 - Custom sidebars
 - Support for post thumbnails
 - Similar posts feature
 - 4 widgetized areas in the footer
 - Customise Front Page 
 - Custom footer
 - Translation Ready 
 

# Basic Setup of the Theme.
-----------------------------------
Fresh installation!

1. Upload the Corpbiz-Pro Theme folder to your wp-content/themes folder.
2. Activate the theme from the WP Dashboard.
3. Done!
=== Images ===

All images in Corpbiz-Pro are licensed under the terms of the GNU GPL.

# Top Navigation Menu:
- Default the page-links start from the left! Use the Menus function in Dashboard/Appearance to rearrange the buttons and build your own Custom-menu. DO NOT USE LONG PAGE NAMES, Maximum 14 letters/numbers incl. spaces!
- Read more here: http://codex.wordpress.org/WordPress_Menu_User_Guide

=============Page Templates======================
1. Contact  Page Tempalte:- Create a page as you do in WordPress and select the page template with the name 'Contact'


===========Front Page Added with the theme=================
1 It has header(logo + menus),Home Featured Image, services,recent comments widgets and footer.

======Site Title and Description=============
Site Title and its description in not shown on home page besides this both are used above each page / post along with the search field.



Corpbiz WordPress Theme bundles the following third-party resources:

Slider Image
URL: https://pixabay.com/en/business-hard-working-autonomous-1067978/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/office-notes-notepad-entrepreneur-620817/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/office-flowers-apple-computer-605503/
Source:http://pixabay.com
License: CC0 Public Domain

Portfolio Image
URL: https://pixabay.com/en/business-hard-working-autonomous-1067978/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/office-working-computer-tablet-1069207/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/apple-mac-computer-desktop-monitor-691282/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/baby-girl-sleep-child-toddler-1151351/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/interior-villa-rendering-1026446/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/monks-path-sunset-landscape-1077839/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/woman-girl-headphones-music-977020/
Source:http://pixabay.com
License: CC0 Public Domain


Client & Team Image
URL: https://pixabay.com/en/business-hard-working-autonomous-1067978/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/business-hard-working-autonomous-1067978/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/computer-notebook-girl-work-people-1120754/
Source:http://pixabay.com
License: CC0 Public Domain

URL: https://pixabay.com/en/entrepreneur-start-start-up-career-696976/
Source:http://pixabay.com
License: CC0 Public Domain

Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
Source: http://fontawesome.io

	
Support
-------
Do you enjoy this theme? Send your ideas - issues - on the theme formn . Thank you!
@Version 1.7.1
1. Fixed header image issue on homepage.

@Version 1.7
1. Updated Turkish Locale.
2. Added Spanish & Russian Locale.

@Version 1.6.9
1. Updated Turkish Locale

@Version 1.6.8
1. Changed widget name

@Version 1.6.7
1. Updated Strings

@Version 1.6.6
1. Added Turkish Locale

@Version 1.6.5
1. Update Pot File.
2. Update Font Awesome Icon Library 4.7.0  

@Version 1.6.4
1. Added Jetpack plugin support and Gallery hover opacity effect css.

@Version 1.6.3
1. Update Font awesome 4.5.0 to 4.6.3
2. Add Title Tag Support
3. Add featured-images, footer-widgets, blog, portfolio Tag.

@Version 1.6.2
1. adding custom header feature.

@Version 1.6.1
1. Styling Issue Solved.

@Version 1.6
1. Resolve home page button styling and typography settings.

@Version 1.5
1. Updated New Slider
2. Styling issue solved.
3. Change slider Setting category Slider to Image slider.
4. Update Font awesome 4.3.0 to 4.5.0
5. Add Default color Skin and Custom color skin.

@Version 1.4
1. Add Customizer.

@Version 1.3
1. Add Google font.
2. Add Post , Category and Image slider.
3. Feature image add aboutus & Conatct us page on header.
4. Theme Data Setup.
5. Custom css sanitization.
6. Remove resize image Setting.
7. Add content.php file.
8. Add Content-portfolio.php file.
9. Add font-awesome Icon URL.
10. Full width layout use afer remove sidebar.
11. Change option panel Name. 

@Version 1.2
1.  Add taxonomy archive portfolio template.
2.  Set none empty field in option pannel setting.
3.  Add max-width property on portfolio image in style.css.
4.  Updated footer customization field in option pannel.
5.  Removed all the unused resources.

@Version 1.1
1.	Pagination issue fixed in category, archive, author, tag.
2.	Filter post according to category, archive, author, tag.
3.	Add custom icon feature in service section on front page, service template and about us template.
4.	Add number of services feature in service section on front page. 

@Version 1.0
released

# --- EOF --- #