<?php
function webriti_scripts()
{	
	$corpbiz_options=theme_data_setup();
	$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), $corpbiz_options );
	wp_enqueue_style( 'corpbiz-style', get_stylesheet_uri() );
	wp_enqueue_style('bootstrap-css', WEBRITI_TEMPLATE_DIR_URI . '/css/bootstrap.css');
	if($current_options['link_color_enable'] == true) {
	custom_light();
	}
	else
	{
	$class=$current_options['theme_color'];
	wp_enqueue_style('default', WEBRITI_TEMPLATE_DIR_URI . '/css/'.$class);
	}
	wp_enqueue_style('theme-menu', WEBRITI_TEMPLATE_DIR_URI . '/css/theme-menu.css');
	wp_enqueue_style('media-responsive', WEBRITI_TEMPLATE_DIR_URI . '/css/media-responsive.css');
	wp_enqueue_style('corpbiz-font-awesome-min',WEBRITI_TEMPLATE_DIR_URI . '/css/font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('theme-element', WEBRITI_TEMPLATE_DIR_URI . '/css/element.css');	
	
	
	wp_enqueue_script('menu', WEBRITI_TEMPLATE_DIR_URI .'/js/menu/menu.js',array('jquery'));
	wp_enqueue_script('bootstrap-min', WEBRITI_TEMPLATE_DIR_URI .'/js/bootstrap.min.js');
	
		
	// Portfolio js and css
	if(is_page_template('portfolio-2-column.php') || is_page_template('portfolio-3-column.php') || is_page_template('portfolio-4-column.php') || is_tax('cor_portfolio_categories'))
	{	wp_enqueue_style('lightbox', WEBRITI_TEMPLATE_DIR_URI . '/css/lightbox.css');	
		wp_enqueue_script('lightbox-2-6', WEBRITI_TEMPLATE_DIR_URI .'/js/lightbox/lightbox-2.6.min.js');
	}	
	if( is_home() || is_front_page() ) {	
		wp_enqueue_style('lightbox', WEBRITI_TEMPLATE_DIR_URI . '/css/lightbox.css');
		wp_enqueue_style('flexslider-css', WEBRITI_TEMPLATE_DIR_URI . '/css/flexslider/flexslider.css');
		wp_enqueue_script('lightbox-2-6', WEBRITI_TEMPLATE_DIR_URI .'/js/lightbox/lightbox-2.6.min.js');
		wp_enqueue_script('jquery-element', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/jquery.flexslider.js');
		//wp_enqueue_script('element', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/flexslider-element.js');
		
		
		/*******nimble slider js*******/
		function slider_js_function() {
			wp_enqueue_script('jquerya4e6', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/jquery.js');
			wp_enqueue_script('superfish-js', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/superfish.js');
			wp_enqueue_script('custom-js', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/custom.js');
			wp_enqueue_script('jquery-flexslider-min-js', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/jquery.flexslider-min.js');
			wp_enqueue_script('jquery-carouFredSel-6-2-1', WEBRITI_TEMPLATE_DIR_URI .'/js/caroufredsel/jquery.carouFredSel-6.2.1-packed.js');
			wp_enqueue_script('caroufredsel-element', WEBRITI_TEMPLATE_DIR_URI .'/js/caroufredsel/caroufredsel-element.js');
		}
		add_action('wp_footer', 'slider_js_function');

	}
	if(is_page_template('service-template.php'))
	{	
		wp_enqueue_script('jquery-carouFredSel-6-2-1', WEBRITI_TEMPLATE_DIR_URI .'/js/caroufredsel/jquery.carouFredSel-6.2.1-packed.js');
		wp_enqueue_script('caroufredsel-element', WEBRITI_TEMPLATE_DIR_URI .'/js/caroufredsel/caroufredsel-element.js');	
	}
	require_once('custom_style.php');
	
}
add_action('wp_enqueue_scripts', 'webriti_scripts');

if ( is_singular() ){ wp_enqueue_script( "comment-reply" );	}
function corpbiz_shortcode_detect() {
    global $wp_query;	
    $posts = $wp_query->posts;
    $pattern = get_shortcode_regex();
	foreach ($posts as $post){
        if (   preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches ) && array_key_exists( 2, $matches ) && in_array( 'button', $matches[2] ) || in_array( 'row', $matches[2] ) || in_array( 'accordian', $matches[2] ) || in_array( 'tabgroup', $matches[2]) || in_array( 'tabs', $matches[2] ) || in_array( 'alert', $matches[2] ) || in_array( 'dropcap', $matches[2] )  || in_array( 'gridsystemlayout', $matches[2] ) || in_array( 'column', $matches[2] ) || in_array( 'heading', $matches[2] )) {
			//wp_enqueue_script('collapse', WEBRITI_TEMPLATE_DIR_URI .'/js/collapse.js');
			wp_enqueue_script('accordion-tab', WEBRITI_TEMPLATE_DIR_URI .'/js/accordion-tab.js',array('jquery'));			
			break;
        }
    }
}
add_action( 'wp', 'corpbiz_shortcode_detect' ); 


// Adding custom enqueue scripts
function corpbiz_custom_scripts(){
$corpbiz_options=theme_data_setup();
$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), $corpbiz_options );	
if($current_options['webrit_custom_css']!='') {  ?>
<style>
<?php echo $current_options['webrit_custom_css']; ?>
</style>
<?php } 
if($current_options['google_analytics']!='') {  ?>
<script type="text/javascript">
<?php echo $current_options['google_analytics']; ?>
</script>
<?php } }
add_action( 'wp_head', 'corpbiz_custom_scripts' ); 

/*--------------------------------------------*/
/*    admin enqueue script function 
/*--------------------------------------------*/

function corpbiz_enqueue_scripts(){
	wp_enqueue_style('bootstrap-css', WEBRITI_TEMPLATE_DIR_URI . '/css/drag-drop.css');
}
add_action( 'admin_enqueue_scripts', 'corpbiz_enqueue_scripts' );


// footer custom script
function footer_custom_script()
{
$corpbiz_options=theme_data_setup(); 
$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), $corpbiz_options);
if($current_options['link_color_enable'] == true) {
custom_light();
}
}
add_action('wp_footer','footer_custom_script');
?>