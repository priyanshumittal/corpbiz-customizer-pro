<?php 
$current_options= wp_parse_args( get_option('corpbiz_options',array()),theme_data_setup());
if($current_options['enable_custom_typography']==true)
{
?>
<style> 
/****** custom typography *********/ 
 .homepage_service_section p,.Client_content p ,.content_responsive_section p,.blog_post_content p,.aboutus_buynow_section p, .about_description_area p,.blog_post_content p,.homepage_top_callout p, .service_area p, .support_content_box p, .entry-content p,
 body p
 {
	font-size:<?php echo $current_options['general_typography_fontsize'].'px'; ?> !important;
	font-family:'Roboto';
	font-style:<?php echo $current_options['general_typography_fontstyle']; ?> ;
	line-height:<?php echo ($current_options['general_typography_fontsize']+5).'px !important'; ?> ;
	font-weight:<?php echo $current_options['general_typography_fontfamily']; ?> !important;
	
}
/*** Menu title */
.navbar .navbar-nav > li > a{
	font-size:<?php echo $current_options['menu_title_fontsize'].'px' ; ?> !important;
	font-family:'Roboto';
	font-weight:<?php echo $current_options['menu_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['menu_title_fontstyle']; ?> !important;
	line-height:<?php echo ($current_options['menu_title_fontsize']+5).'px !important'; ?> ;
}
/*** post and Page title */
.post_title_wrapper h2 a, .entry-title {
	font-size:<?php echo $current_options['post_title_fontsize'].'px'; ?> !important;
	font-family:'Roboto';
	font-weight:<?php echo $current_options['post_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['post_title_fontstyle']; ?>;
	line-height:<?php echo ($current_options['post_title_fontsize']+5).'px !important'; ?> ;
}
/*** service title */
.homepage_service_section h2, .service_area h2
{
	font-size:<?php echo $current_options['service_title_fontsize'].'px'; ?> !important;
	font-family:'Roboto';
	font-weight:<?php echo $current_options['service_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['service_title_fontstyle']; ?>;
	line-height:<?php echo ($current_options['service_title_fontsize']+5).'px !important'; ?> ;
}

/******** portfolio title ********/
.corpo_home_portfolio_showcase_icons h4,.portfolio_caption a { 
	font-size:<?php echo $current_options['portfolio_title_fontsize'].'px'; ?> !important;
	font-family:'Roboto';
	font-weight:<?php echo $current_options['portfolio_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['portfolio_title_fontstyle']; ?>;
	line-height:<?php echo ($current_options['portfolio_title_fontsize']+5).'px !important'; ?> ;
}
/******* footer widget title*********/
.footer_widget_title, .sidebar_widget_title h2, .widget-title{
	font-size:<?php echo $current_options['widget_title_fontsize'].'px'; ?> !important;
	font-family:'Roboto';
	font-weight:<?php echo $current_options['widget_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['widget_title_fontstyle']; ?>;
	line-height:<?php echo ($current_options['widget_title_fontsize']+5).'px !important'; ?> ;
}
.footer_callout_area h2{
	font-size:<?php echo $current_options['calloutarea_title_fontsize'].'px'; ?> !important;
	font-family:'Roboto';
	font-weight:<?php echo $current_options['calloutarea_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['calloutarea_title_fontstyle']; ?>;
	line-height:<?php echo ($current_options['calloutarea_title_fontsize']+5).'px !important'; ?> ;
}
.footer_callout_area p{
	font-size:<?php echo $current_options['calloutarea_description_fontsize'].'px'; ?> !important;
	font-family:'Roboto';
	font-weight:<?php echo $current_options['calloutarea_description_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['calloutarea_description_fontstyle']; ?>;
	line-height:<?php echo ($current_options['calloutarea_description_fontsize']+5).'px !important'; ?> ;
}
</style>
<?php } ?>