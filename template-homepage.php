<?php 
		/**
		Template Name: Home Page
		*/
		$current_options=theme_data_setup();
		$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), $current_options );
		get_header();
		get_template_part('index','slider');
		$data =is_array($current_options['front_page_data']) ? $current_options['front_page_data'] : explode(",",$current_options['front_page_data']);
		if($data) 
		{
			foreach($data as $key=>$value)
			{			
				switch($value) 
				{	case 'site-info': 
					//****** get index service  ********
					get_template_part('index', 'site-info');
					break;
					
					case 'service': 
					//****** get index service  ********
					get_template_part('index', 'service');
					break;
					
					case 'project-slider':
					//****** get index project  ********
					get_template_part('index', 'project-slider');				
					break;
					
					case 'portfolio':
					//****** get index project  ********
					get_template_part('index', 'portfolio');				
					break;
					
					case 'testimonial': 			
					//****** get index recent blog  ********
					get_template_part('index', 'testimonial');				
					break; 	
					
					case 'help-support': 			
					//****** get index testimonials  ********
					get_template_part('index', 'help-support');					
					break;
					
					case 'blog': 			
					//****** get index blog  ********
					get_template_part('index', 'blog');					
					break;
					
					case 'call-out-area': 			
					//****** get index call out area ********
					get_template_part('index', 'call-out-area');					
					break; 
				}
			}
		 	
	get_footer(); 
	}
?>