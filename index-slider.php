<?php
$corpbiz_options=theme_data_setup(); 
$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), $corpbiz_options ); 
$settings= array();
$settings=array('animation'=>$current_options['animation'],'animationSpeed'=>$current_options['animationSpeed'],'slideshowSpeed' =>$current_options['slideshowSpeed']);

wp_register_script('corpbiz-slider',get_template_directory_uri().'/js/front-page/slider.js',array('jquery'));
wp_localize_script('corpbiz-slider','slider_settings',$current_options);
wp_enqueue_script('corpbiz-slider'); ?>
<!--Flex Slider-->	
<?php if($current_options['home_banner_enabled'] == true) { ?>

<div class="main_slider">	
	<div class="flexslider">
        <div class="flex-viewport">
			<ul class="slides">
				<?php if( $current_options['slider_image_one_title'] != '' || $current_options['slider_image_one_description'] !='' || $current_options['slider_one_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_one']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_one']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_one_title']!='' ? '<h1>'.$current_options['slider_image_one_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_one_description']!='' ? '<p>'.$current_options['slider_image_one_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_one_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_one_readmore_link']; ?>" <?php if( $current_options['slider_one_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_one_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>				
				
				<?php if( $current_options['slider_image_two_title'] != '' || $current_options['slider_image_two_description'] !='' || $current_options['slider_two_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_two']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_two']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_two_title']!='' ? '<h1>'.$current_options['slider_image_two_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_two_description']!='' ? '<p>'.$current_options['slider_image_two_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_two_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_two_readmore_link']; ?>" <?php if( $current_options['slider_two_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_two_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>
				
				<?php if( $current_options['slider_image_three_title'] != '' || $current_options['slider_image_three_description'] !='' || $current_options['slider_three_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_three']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_three']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_three_title']!='' ? '<h1>'.$current_options['slider_image_three_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_three_description']!='' ? '<p>'.$current_options['slider_image_three_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_three_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_three_readmore_link']; ?>" <?php if( $current_options['slider_three_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_three_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>
				
				<?php if( $current_options['slider_image_four_title'] != '' || $current_options['slider_image_four_description'] !='' || $current_options['slider_four_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_four']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_four']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_four_title']!='' ? '<h1>'.$current_options['slider_image_four_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_four_description']!='' ? '<p>'.$current_options['slider_image_four_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_four_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_four_readmore_link']; ?>" <?php if( $current_options['slider_four_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_four_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>
				
				<?php if( $current_options['slider_image_five_title'] != '' || $current_options['slider_image_five_description'] !='' || $current_options['slider_five_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_five']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_five']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_five_title']!='' ? '<h1>'.$current_options['slider_image_five_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_five_description']!='' ? '<p>'.$current_options['slider_image_five_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_five_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_five_readmore_link']; ?>" <?php if( $current_options['slider_five_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_five_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>
				
				<?php if( $current_options['slider_image_six_title'] != '' || $current_options['slider_image_six_description'] !='' || $current_options['slider_six_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_six']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_six']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_six_title']!='' ? '<h1>'.$current_options['slider_image_six_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_six_description']!='' ? '<p>'.$current_options['slider_image_six_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_six_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_six_readmore_link']; ?>" <?php if( $current_options['slider_six_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_six_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>
				
				<?php if( $current_options['slider_image_six_title'] != '' || $current_options['slider_image_six_description'] !='' || $current_options['slider_six_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_six']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_six']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_six_title']!='' ? '<h1>'.$current_options['slider_image_six_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_six_description']!='' ? '<p>'.$current_options['slider_image_six_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_six_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_six_readmore_link']; ?>" <?php if( $current_options['slider_six_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_six_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>
				
				<?php if( $current_options['slider_image_seven_title'] != '' || $current_options['slider_image_seven_description'] !='' || $current_options['slider_seven_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_seven']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_seven']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_seven_title']!='' ? '<h1>'.$current_options['slider_image_seven_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_seven_description']!='' ? '<p>'.$current_options['slider_image_seven_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_seven_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_seven_readmore_link']; ?>" <?php if( $current_options['slider_seven_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_seven_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>
				
				<?php if( $current_options['slider_image_eight_title'] != '' || $current_options['slider_image_eight_description'] !='' || $current_options['slider_eight_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_eight']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_eight']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_eight_title']!='' ? '<h1>'.$current_options['slider_image_eight_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_eight_description']!='' ? '<p>'.$current_options['slider_image_seven_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_eight_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_eight_readmore_link']; ?>" <?php if( $current_options['slider_eight_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_eight_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>
				
				<?php if( $current_options['slider_image_nine_title'] != '' || $current_options['slider_image_nine_description'] !='' || $current_options['slider_nine_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_nine']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_nine']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_nine_title']!='' ? '<h1>'.$current_options['slider_image_nine_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_nine_description']!='' ? '<p>'.$current_options['slider_image_nine_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_nine_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_nine_readmore_link']; ?>" <?php if( $current_options['slider_nine_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_nine_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>
				
				<?php if( $current_options['slider_image_ten_title'] != '' || $current_options['slider_image_ten_description'] !='' || $current_options['slider_ten_readmore_link'] != '' ) { ?>
				<li>
					<?php if($current_options['slider_image_ten']!=''): ?>
					<img class="img-responsive" src="<?php echo $current_options['slider_image_ten']; ?>">
					<?php else: ?>
					<img class="img-responsive" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/slides/no-image.jpg">
					<?php endif; ?>
					<div class="container slide-caption">
						<div class="overlay">
						<?php
						echo ( $current_options['slider_image_ten_title']!='' ? '<h1>'.$current_options['slider_image_ten_title'].'</h1>' : '' ); 
						echo ( $current_options['slider_image_ten_description']!='' ? '<p>'.$current_options['slider_image_ten_description'].'</p>' : '' );
						?>
						</div>
						<?php if($current_options['slider_ten_readmore_text'] != '') { ?>
						<div class="flex-btn-div">
						<a href="<?php echo $current_options['slider_ten_readmore_link']; ?>" <?php if( $current_options['slider_ten_readmore_ink_target'] == true ) { echo "target='_blank'"; } ?> class="flex-btn"><?php echo $current_options['slider_ten_readmore_text']; ?></a>
						</div>
						<?php } ?>						
                    </div>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>	
	<!--/Flex Slider-->
<?php } if ( ! is_front_page() || !is_home() &&  !is_page_template('template-homepage.php') ) : ?>
</div>
<?php endif; ?>