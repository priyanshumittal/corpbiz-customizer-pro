<?php
/**Theme Name	: Corpbiz-Pro
 * Theme Core Functions and Codes
*/	
	/**Includes reqired resources here**/
	define('WEBRITI_TEMPLATE_DIR_URI',get_template_directory_uri());	
	define('WEBRITI_TEMPLATE_DIR',get_template_directory());
	define('WEBRITI_THEME_FUNCTIONS_PATH',WEBRITI_TEMPLATE_DIR.'/functions');	
	define('WEBRITI_THEME_OPTIONS_PATH',WEBRITI_TEMPLATE_DIR_URI.'/functions/theme_options');
	
	// Add default data file 
	require_once('theme_setup_data.php');
	
	// Adding customizer files
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_home_page.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_slider.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_typography.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_footer_customization.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_texonomy_archive.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_layout_manager.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-template.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_theme_style.php');
	
	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/default_menu_walker.php'); 
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/webriti_nav_walker.php'); 
	require_once( WEBRITI_THEME_FUNCTIONS_PATH . '/scripts/scripts.php'); 
	require_once( WEBRITI_THEME_FUNCTIONS_PATH . '/font/font.php'); 
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/post-type/custom-post-type.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/meta-box/post-meta.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/taxonomies/taxonomies.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/excerpt/excerpt.php');
	// custom widget files
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/custom-sidebar.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/header-left-info.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/header-right-social.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/webriti-footer-contact-widgets.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/corpbiz-latest-widget.php');
	
	
	
	
	//require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/corpbiz-latest-widget.php');	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/pagination/webriti_pagination.php'); 
	//Template-tag
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/template-tags.php'); 
	//Webriti shortcodes
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/shortcodes/shortcodes.php' ); //for shortcodes 
	//Custom light
	require( WEBRITI_TEMPLATE_DIR . '/css/custom-light.php');
	
	//content width
	if ( ! isset( $content_width ) ) $content_width = 700;//In PX		
	//wp title tag starts here
	function webriti_head( $title, $sep )
	{	global $paged, $page;		
		if ( is_feed() )
			return $title;
		// Add the site name.
		$title .= get_bloginfo( 'name' );
		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( _e( 'Page', 'corpbiz' ), max( $paged, $page ) );
		return $title;
	}	
	add_filter( 'wp_title', 'webriti_head', 10, 2);
	
	add_action( 'after_setup_theme', 'webriti_setup' ); 	
	function webriti_setup()
	{	// Load text domain for translation-ready
		load_theme_textdomain( 'corpbiz', WEBRITI_THEME_FUNCTIONS_PATH . '/lang' );
		add_theme_support( 'post-thumbnails' ); //supports featured image
		// This theme uses wp_nav_menu() in one location.
		register_nav_menu( 'primary', __( 'Primary Menu', 'corpbiz' ) ); //Navigation
		register_nav_menu( 'secondary', __( 'Footer Menu', 'corpbiz' ) ); //Navigation
		// theme support 	
		//$args = array('default-color' => '#ffffff');
		//add_theme_support( 'custom-background', $args ); 
		add_theme_support( 'automatic-feed-links');
		add_theme_support( 'title-tag' );
		
		// custom header
			$args = array(
			'default-image'		=>  get_template_directory_uri() .'/images/sub-header.jpg',
			'width'			=> '1600',
			'height'		=> '500',
			'flex-height'		=> false,
			'flex-width'		=> false,
			'header-text'		=> true,
			'default-text-color'	=> '#143745'
		);
		add_theme_support( 'custom-header', $args );
		
	} 
	function add_to_author_profile( $contactmethods ) {
		$contactmethods['youtube_profile'] = __( 'Youtube URL', 'corpbiz' );
		$contactmethods['google_profile'] = __( 'GooglePlus URL', 'corpbiz' );
		$contactmethods['twitter_profile'] = __( 'Twitter URL', 'corpbiz' );
		$contactmethods['facebook_profile'] = __( 'Facebook URL', 'corpbiz' );
		$contactmethods['linkedin_profile'] = __( 'LinkedIn URL', 'corpbiz' );
		return $contactmethods;
	}
	add_filter( 'user_contactmethods', 'add_to_author_profile', 10, 1);
	
	add_filter('get_avatar','add_gravatar_class');
	function add_gravatar_class($class) {
		$class = str_replace("class='avatar", "class='img-responsive comment_img img-circle media-object", $class);
		return $class;
	}
	// Read more tag to formatting in blog page 	
	function new_content_more($more)
	{  global $post;
		return '<div class="blog-btn-col"><a href="' . get_permalink() . "\" class=\"blog-btn\">".__('Read More','corpbiz')."</a></div>";
	}   
	add_filter( 'the_content_more_link', 'new_content_more' );
	
	
	
	// slider excerpt function
	function get_slider_blog_excerpt()
	{
		global $post;
		$excerpt = get_the_content();
		$excerpt = strip_tags(preg_replace(" (\[.*?\])",'',$excerpt));
		$excerpt = strip_shortcodes($excerpt);
		$original_len = strlen($excerpt);
		$excerpt = substr($excerpt, 0, 60);
		$len=strlen($excerpt);
		if($original_len>275) {
		$excerpt = $excerpt;
		return $excerpt;
		}
		else
		{ return $excerpt; }
	}
?>