<?php 

// Adding customizer home page settings

function corpbiz_home_page_customizer( $wp_customize ){


// list control categories	
if ( ! class_exists( 'WP_Customize_Control' ) ) return NULL;

 class Category_Dropdown_Custom_Control1 extends WP_Customize_Control
 {
    private $cats = false;
	
    public function __construct($wp_customize, $id, $args = array(), $options = array())
    {
        $this->cats = get_categories($options);
        parent::__construct( $wp_customize, $id, $args );
    }

    public function render_content()
       {
            if(!empty($this->cats))
            {
                ?>
                    <label>
                      <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                      <select multiple="multiple" <?php $this->link(); ?>>
                           <?php
                                foreach ( $this->cats as $cat )
                                {
                                    printf('<option value="%s" %s>%s</option>', $cat->term_id, selected($this->value(), $cat->term_id, false), $cat->name);
                                }
                           ?>
                      </select>
                    </label>
                <?php
            }
       }
 }
// list contro categories

	
	/* Home Page Panel */
	$wp_customize->add_panel( 'home_page', array(
		'priority'       => 450,
		'capability'     => 'edit_theme_options',
		'title'      => __('Homepage section setting','corpbiz'),
	) );
	
	/* Quick Start */
	$wp_customize->add_section( 'quick_start' , array(
		'title'      => __('Quick start', 'corpbiz'),
		'panel'  => 'home_page',
		'priority'   => 0,
   	) );
	
	$wp_customize->add_setting(
	'corpbiz_options[text_title]', array(
        'default'        => true,
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[text_title]', array(
        'label'   => __('Enable Logo Text', 'corpbiz'),
        'section' => 'quick_start',
        'type'    => 'checkbox',
		'priority'   => 2,
    )); // enable / disable logo text
	
	$wp_customize->add_setting(
	'corpbiz_options[upload_image_logo]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    ));
	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'corpbiz_options[upload_image_logo]', array(
      'label'    => __( 'Custom Logo', 'corpbiz' ),
      'section'  => 'quick_start',
	  'priority'   => 3,
     ))
	 ); // theme logo upload
	 
	 $wp_customize->add_setting(
	'corpbiz_options[width]', array(
        'default'        => 100,
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[width]', array(
        'label'   => __('Enter Logo Width', 'corpbiz'),
        'section' => 'quick_start',
        'type'    => 'text',
		'priority'   => 4,
    )); // logo width
	
	$wp_customize->add_setting(
	'corpbiz_options[height]', array(
        'default'        => 50,
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[height]', array(
        'label'   => __('Enter Logo Height', 'corpbiz'),
        'section' => 'quick_start',
        'type'    => 'text',
		'priority'   => 5,
    )); // logo hieght
	
	
	$wp_customize->add_setting(
	'corpbiz_options[upload_image_favicon]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    ));
	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'corpbiz_options[upload_image_favicon]', array(
      'label'    => __( 'Site favicon', 'corpbiz' ),
      'section'  => 'quick_start',
	  'priority'   => 6,
     ))
	 ); // favicon icon
	 
	 $wp_customize->add_setting(
	'corpbiz_options[google_analytics]', array(
		'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[google_analytics]', array(
        'label'   => __('Google tracking code', 'corpbiz'),
        'section' => 'quick_start',
        'type'    => 'textarea',
		'priority'   => 7,
    )); // Google analysis code
	
	$wp_customize->add_setting(
	'corpbiz_options[webrit_custom_css]', array(
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[webrit_custom_css]', array(
        'label'   => __('Custom CSS', 'corpbiz'),
        'section' => 'quick_start',
        'type'    => 'textarea',
		'priority'   => 8,
    )); // custom css
	
	
	
	
	/* Site Info Settings */
	$wp_customize->add_section( 'site_info_settings' , array(
		'title'      => __('Siteinfo setting', 'corpbiz'),
		'panel'  => 'home_page',
		'priority'   => 1,
   	) );
	
	$wp_customize->add_setting(
	'corpbiz_options[site_title_one]', array(
        'default'        => '40+',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[site_title_one]', array(
        'label'   => __('Title One', 'corpbiz'),
        'section' => 'site_info_settings',
        'type'    => 'text',
		'priority'   => 1,
    )); // site info title one
	
	$wp_customize->add_setting(
	'corpbiz_options[site_title_two]', array(
        'default'        => __('Sample pages', 'corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[site_title_two]', array(
        'label'   => __('Title Two', 'corpbiz'),
        'section' => 'site_info_settings',
        'type'    => 'text',
		'priority'   => 2,
    )); // site info title two
	
	$wp_customize->add_setting(
	'corpbiz_options[site_description]', array(
        'default'        => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam scelerisque faucibus risus non iaculis. Fusce a augue ante, pellentesque pretium erat. Fusce in turpis in velit tempor pretium. Integer a leo libero',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[site_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'site_info_settings',
        'type'    => 'textarea',
		'priority'   => 2,
    )); // site info description
	
	$wp_customize->add_setting(
	'corpbiz_options[siteinfo_button_one_text]', array(
        'default'        => __('Read More','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[siteinfo_button_one_text]', array(
        'label'   => __('Button Text', 'corpbiz'),
        'section' => 'site_info_settings',
        'type'    => 'text',
		'priority'   => 3,
    )); // button one
	
	$wp_customize->add_setting(
	'corpbiz_options[siteinfo_button_one_target]', array(
        'default'        => true,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[siteinfo_button_one_target]', array(
        'label'   => __('Open window in new tab', 'corpbiz'),
        'section' => 'site_info_settings',
        'type'    => 'checkbox',
		'priority'   => 4,
    )); // button one target
	
	$wp_customize->add_setting(
	'corpbiz_options[siteinfo_button_one_link]', array(
        'default'        => '#',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[siteinfo_button_one_link]', array(
        'label'   => __('Button Link', 'corpbiz'),
        'section' => 'site_info_settings',
        'type'    => 'text',
		'priority'   => 5,
    )); // button one target url
	
	$wp_customize->add_setting(
	'corpbiz_options[siteinfo_button_two_text]', array(
        'default'        => __('View portfolio','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[siteinfo_button_two_text]', array(
        'label'   => __('Button Text', 'corpbiz'),
        'section' => 'site_info_settings',
        'type'    => 'text',
		'priority'   => 6,
    )); // button two
	
	$wp_customize->add_setting(
	'corpbiz_options[siteinfo_button_two_target]', array(
        'default'        => true,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[siteinfo_button_two_target]', array(
        'label'   => __('Open window in new tab', 'corpbiz'),
        'section' => 'site_info_settings',
        'type'    => 'checkbox',
		'priority'   => 7,
    )); // button two target
	
	$wp_customize->add_setting(
	'corpbiz_options[siteinfo_button_two_link]', array(
        'default'        => '#',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[siteinfo_button_two_link]', array(
        'label'   => __('Button Link', 'corpbiz'),
        'section' => 'site_info_settings',
        'type'    => 'text',
		'priority'   => 8,
    )); // button two target url
	
	
	/* Service Settings */
	$wp_customize->add_section( 'service_settings' , array(
		'title'      => __('Service settings', 'corpbiz'),
		'panel'  => 'home_page',
		'priority'   => 2,
   	) );
	
	$wp_customize->add_setting(
	'corpbiz_options[service_list]', array(
        'default'        => 4,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[service_list]', array(
        'label'   => __('Number of services on service section', 'corpbiz'),
        'section' => 'service_settings',
        'type'    => 'select',
		'priority'   => 1,
		'choices'=>array( 4=>4,8=>8,12=>12,16=>16,20=>20,24=>24)
    )); // service list
	
	$wp_customize->add_setting(
	'corpbiz_options[home_service_title]', array(
        'default'        => __('Our nice services','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_service_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'service_settings',
        'type'    => 'text',
		'priority'   => 2,
    )); // service title
	
	$wp_customize->add_setting(
	'corpbiz_options[home_service_description]', array(
        'default'        => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam scelerisque faucibus risus non iaculis.',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_service_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'service_settings',
        'type'    => 'textarea',
		'priority'   => 3,
    )); 
	
	class WP_service_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
    <a href="<?php bloginfo ( 'url' );?>/wp-admin/edit.php?post_type=corpbiz_service" class="button"  target="_blank"><?php _e( 'Click here to add service', 'corpbiz' ); ?></a>
    <?php
    }
	}

	$wp_customize->add_setting(
		'service',
		array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		)	
	);
	$wp_customize->add_control( new WP_service_Customize_Control( $wp_customize, 'service', array(	
			'section' => 'service_settings',
			'priority'   => 500,
		))
	);
	
	// service description
	
	/* Project Slider */ 
	
	$wp_customize->add_section( 'project_settings' , array(
		'title'      => __('Project slider settings', 'corpbiz'),
		'panel'  => 'home_page',
		'priority'   => 3,
   	) );
	
	
	class WP_project_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
    <a href="<?php bloginfo ( 'url' );?>/wp-admin/edit.php?post_type=corpbiz_project" class="button"  target="_blank"><?php _e( 'Click here to add project slider', 'corpbiz' ); ?></a>
    <?php
    }
	}
	
	$wp_customize->add_setting(
		'project',
		array(
			'default' => '',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		)	
	);
	$wp_customize->add_control( new WP_project_Customize_Control( $wp_customize, 'project', array(	
			'section' => 'project_settings',
			'priority'   => 500,
		))
	);
	
	
	
	
	
	
	//* Project Setting *//
	/* Portfolio Settings */
	$wp_customize->add_section( 'portfolio_settings' , array(
		'title'      => __('Portfolio settings', 'corpbiz'),
		'panel'  => 'home_page',
		'priority'   => 4,
   	) );
	
	$wp_customize->add_setting(
	'corpbiz_options[portfolio_title]', array(
        'default'        => __('Our work speaks thousand words','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[portfolio_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'portfolio_settings',
        'type'    => 'text',
		'priority'   => 1,
    )); // portfolio title
	
	$wp_customize->add_setting(
	'corpbiz_options[portfolio_description]', array(
        'default'        => __('We have successfully completed over 2500 projects in mobile and web. Here are few of them.','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[portfolio_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'portfolio_settings',
        'type'    => 'textarea',
		'priority'   => 2,
    )); 
	
	class WP_portfolio_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
    <a href="<?php bloginfo ( 'url' );?>/wp-admin/edit.php?post_type=corpbiz_portfolio" class="button"  target="_blank"><?php _e( 'Click here to add portfolio', 'corpbiz' ); ?></a>
    <?php
    }
	}

	$wp_customize->add_setting(
		'portfolio',
		array(
			'default' => '',
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		)	
	);
	$wp_customize->add_control( new WP_portfolio_Customize_Control( $wp_customize, 'portfolio', array(	
			'section' => 'portfolio_settings',
			'priority'   => 500,
		))
	);
	
	
	// portfolio description
	
	
	/* Client Settings */
	$wp_customize->add_section( 'client_settings' , array(
		'title'      => __('Client settings', 'corpbiz'),
		'panel'  => 'home_page',
		'priority'   => 5,
   	) );
	
	$wp_customize->add_setting(
	'corpbiz_options[home_client_title]', array(
        'default'        => __('What our clients says','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_client_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'client_settings',
        'type'    => 'text',
		'priority'   => 1,
    )); // client title
	
	$wp_customize->add_setting(
	'corpbiz_options[home_client_desciption]', array(
        'default'        => __('lets see what we hear from our valuable clients','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_client_desciption]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'client_settings',
        'type'    => 'textarea',
		'priority'   => 2,
    )); // client description
	
	$wp_customize->add_setting(
	'corpbiz_options[client_list]', array(
        'default'        => 4,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[client_list]', array(
        'label'   => __('Number of client on client section', 'corpbiz'),
        'section' => 'client_settings',
        'type'    => 'select',
		'priority'   => 3,
		'choices'=>array( 2=>2,4=>4,6=>6,8=>8,10=>10,12=>12,14=>14,16=>16,18=>18,20=>20,22=>22)
    )); // client list
	
	
	//Client link
	class WP_client_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
    <a href="<?php bloginfo ( 'url' );?>/wp-admin/edit.php?post_type=corpbiz_testimonial" class="button"  target="_blank"><?php _e( 'Click here to add client', 'corpbiz' ); ?></a>
    <?php
    }
	}

	$wp_customize->add_setting(
		'client',
		array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		)	
	);
	$wp_customize->add_control( new WP_client_Customize_Control( $wp_customize, 'client', array(	
			'label' => __('Discover corpbiz Pro','corpbiz'),
			'section' => 'client_settings',
			'priority'   => 500,
		))
	);
	
	
	
	/* Theme Support Settings */
	$wp_customize->add_section( 'theme_support' , array(
		'title'      => __('Theme support settings', 'corpbiz'),
		'panel'  => 'home_page',
		'priority'   => 6,
   	) );
	
	$wp_customize->add_setting(
	'corpbiz_options[home_theme_support_bg]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    ));
	$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'corpbiz_options[home_theme_support_bg]', array(
      'label'    => __( 'Background Image', 'corpbiz' ),
      'section'  => 'theme_support',
	  'priority'   => 1,
     ))
	 ); // theme background image
	 
	 $wp_customize->add_setting(
	'corpbiz_options[home_theme_support_title]', array(
        'default'        => __('We are here to help you','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_theme_support_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 2,
    )); // section title
	
	$wp_customize->add_setting(
	'corpbiz_options[home_theme_support_description]', array(
        'default'        => __('24+7 hours support by us','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_theme_support_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'textarea',
		'priority'   => 3,
    )); // section description
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_icon_one]', array(
        'default'        => 'fa-meh-o',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_icon_one]', array(
        'label'   => __('Icon', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 4,
    )); // icon one
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_title_one]', array(
        'default'        => __('Need support','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_title_one]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 5,
    )); // title one
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_desciption_one]', array(
        'default'        => 'Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_desciption_one]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'textarea',
		'priority'   => 6,
    )); // description one
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_learn_more_text_one]', array(
        'default'        => __('Read More','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_learn_more_text_one]', array(
        'label'   => __('Button Text', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 7,
    )); // learn more button 1
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_learn_more_target_one]', array(
        'default'        => true,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_learn_more_target_one]', array(
        'label'   => __('Open link in new tab', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'checkbox',
		'priority'   => 8,
    )); // learn more target 1
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_learn_more_link_one]', array(
        'default'        => '#',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_learn_more_link_one]', array(
        'label'   => __('Button Link', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 9,
    )); // learn more button link 1
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_icon_two]', array(
        'default'        => 'fa-list-ol',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_icon_two]', array(
        'label'   => __('Icon', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 10,
    )); // icon two
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_title_two]', array(
        'default'        => 'Check Our Forum',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_title_two]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 11,
    )); // title two
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_desciption_two]', array(
        'default'        => 'Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in',        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_desciption_two]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'textarea',
		'priority'   => 12,
    )); // description two
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_learn_more_text_two]', array(
        'default'        => __('Read More','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_learn_more_text_two]', array(
        'label'   => __('Button Text', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 13,
    )); // learn more button two
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_learn_more_target_two]', array(
        'default'        => true,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_learn_more_target_two]', array(
        'label'   => __('Open link in new tab', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'checkbox',
		'priority'   => 14,
    )); // learn more target two
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_learn_more_link_two]', array(
        'default'        => '#',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_learn_more_link_two]', array(
        'label'   => __('Button Link', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 15,
    )); // learn more button link two
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_icon_three]', array(
        'default'        => 'fa-support',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_icon_three]', array(
        'label'   => __('Icon', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 16,
    )); // icon three
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_title_three]', array(
        'default'        => __('Get updated','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_title_three]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 17,
    )); // title three
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_desciption_three]', array(
        'default'        => 'Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_desciption_three]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'textarea',
		'priority'   => 18,
    )); // description three
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_learn_more_text_three]', array(
        'default'        => __('Read More','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_learn_more_text_three]', array(
        'label'   => __('Button Text', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 19,
    )); // learn more button three
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_learn_more_target_three]', array(
        'default'        => true,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_learn_more_target_three]', array(
        'label'   => __('Open link in new tab', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'checkbox',
		'priority'   => 20,
    )); // learn more target three
	
	$wp_customize->add_setting(
	'corpbiz_options[home_support_learn_more_link_three]', array(
        'default'        => '#',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_support_learn_more_link_three]', array(
        'label'   => __('Button Link', 'corpbiz'),
        'section' => 'theme_support',
        'type'    => 'text',
		'priority'   => 21,
    )); // learn more button link three
	
	
	/* Footer Callout Settings */
	$wp_customize->add_section( 'footer_callout' , array(
		'title'      => __('Footer callout', 'corpbiz'),
		'panel'  => 'home_page',
		'priority'   => 8,
   	) );
	
	$wp_customize->add_setting(
	'corpbiz_options[call_out_title]', array(
        'default'        => __('Get your app ideas transformed into reality','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[call_out_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'footer_callout',
        'type'    => 'text',
		'priority'   => 1,
    )); // callout title
	
	$wp_customize->add_setting(
	'corpbiz_options[call_out_text]', array(
        'default'        =>  'Lorem ipsum dolor sit amet, consectetur faucibus risus non iaculis. Fusce a augue Fusce in turpis in.',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[call_out_text]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'footer_callout',
        'type'    => 'textarea',
		'priority'   => 2,
    )); // callout text
	
	$wp_customize->add_setting(
	'corpbiz_options[call_out_button_text]', array(
        'default'        => __('Read More','corpbiz'),
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[call_out_button_text]', array(
        'label'   => __('Button Text', 'corpbiz'),
        'section' => 'footer_callout',
        'type'    => 'text',
		'priority'   => 3,
    )); // callout button text
	
	$wp_customize->add_setting(
	'corpbiz_options[call_out_button_link_target]', array(
        'default'        => true,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[call_out_button_link_target]', array(
        'label'   => __('Open link in new tab', 'corpbiz'),
        'section' => 'footer_callout',
        'type'    => 'checkbox',
		'priority'   => 4,
    )); // callout button target
	
	$wp_customize->add_setting(
	'corpbiz_options[call_out_button_link]', array(
        'default'        => '#',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[call_out_button_link]', array(
        'label'   => __('Button Link', 'corpbiz'),
        'section' => 'footer_callout',
        'type'    => 'text',
		'priority'   => 5,
    )); // callout button URL
	
	
	
	
	/*Blog Heading section*/
	$wp_customize->add_section(
        'blog_setting',
        array(
            'title' => __('Blog settings','corpbiz'),
			'priority'   => 7,
			'panel'=>'home_page'
			)
    );
	
	$wp_customize->add_setting(
    'corpbiz_options[blog_title]',
    array(
        'default' => __('From blog','corpbiz'),
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		));	
	$wp_customize->add_control( 'corpbiz_options[blog_title]',array(
    'label'   => __('Title','corpbiz'),
    'section' => 'blog_setting',
	 'type' => 'text',));
	
	$wp_customize->add_setting(
    'corpbiz_options[blog_description]',
    array(
        'default' => 'Lorem ipsum dolor sit ametconsectetuer adipiscing elit.',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		));	
	$wp_customize->add_control( 'corpbiz_options[blog_description]',array(
    'label'   => __('Description','corpbiz'),
    'section' => 'blog_setting',
	 'type' => 'textarea',));	
	 
	 
	 // add section to manage featured Latest blog on category basis	
	$wp_customize->add_setting(
	'corpbiz_options[blog_selected_category_id]', array(
        'default'        => 1,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control( new Category_Dropdown_Custom_Control1( $wp_customize,'corpbiz_options[blog_selected_category_id]', array(
    'label'   => __('Select category for latest blog','corpbiz'),
    'section' => 'blog_setting',
    'settings'   =>  'corpbiz_options[blog_selected_category_id]',
	) ) ); // blog category
	
	$wp_customize->add_setting(
    'corpbiz_options[post_display_count]',
    array(
		'type' => 'option',
        'default' => 3,
		'sanitize_callback' => 'sanitize_text_field',
    ));

	$wp_customize->add_control(
    'corpbiz_options[post_display_count]',
    array(
        'type' => 'select',
        'label' => __('Select number of post','corpbiz'),
        'section' => 'blog_setting',
		 'choices' => array(3=>3, 6=>6, 9=>9, 12=>12, 15=>15),
		));
	
	 
	
}
add_action( 'customize_register', 'corpbiz_home_page_customizer' );


function get_categories_select() {
  $teh_cats = get_categories();
  $results;
 
  $count = count($teh_cats);
  for ($i=0; $i < $count; $i++) { 
    if (isset($teh_cats[$i]))
      $results[$teh_cats[$i]->slug] = $teh_cats[$i]->name;
    else
      $count++;
  }
  return $results;
}