<!--Homepage Service Section-->
<?php 
$corpbiz_options=theme_data_setup(); 
$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), $corpbiz_options ); ?>
<div class="service_section">
<div class="container">
	<div class="row">
		<div class="section_title">
			<?php if($current_options['home_service_title'] !="") { ?>
			<h1 class="widget-title"><?php echo $current_options['home_service_title']; ?></h1>
			<?php } ?>
			<?php if($current_options['home_service_description'] !="") { ?>
			<p> <?php echo $current_options['home_service_description']; ?> </p>
			<?php } ?>
		</div>	
	</div>
	
	<div class="row">
		<?php
			$i=1;
			$total_services = $current_options['service_list'];			
			$args = array( 'post_type' => 'corpbiz_service','posts_per_page' =>$total_services); 	
			$service = new WP_Query( $args ); 
			if( $service->have_posts() )
			{ while ( $service->have_posts() ) : $service->the_post(); ?>
			
			<div class="col-md-3 col-sm-6">	
				<div class="service_area">
				<?php if(get_post_meta( get_the_ID(),'meta_service_link', true )) 
					{ $meta_service_link=get_post_meta( get_the_ID(),'meta_service_link', true ); }
					else
					{ $meta_service_link = ""; }						
					?>
					<?php if(has_post_thumbnail()){  ?>	
						<div class="service_box">
							<?php if($meta_service_link){
									$defalt_arg =array('class' => "img-responsive"); ?> 
									<a href="<?php echo $meta_service_link;  ?>" <?php if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo 'target="_blank"'; } ?>> <?php the_post_thumbnail('', $defalt_arg); ?> </a>
							<?php } else {
									$defalt_arg =array('class' => "img-responsive"); 
									the_post_thumbnail('', $defalt_arg);
							} ?>
						</div>
					<?php } else {
						if(get_post_meta( get_the_ID(),'service_icon_image', true )) {?>
						<div class="service_box">
						<?php if($meta_service_link){ ?>
						<a href="<?php echo $meta_service_link;  ?>" <?php if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo 'target="_blank"'; } ?>> <i class="fa <?php echo get_post_meta( get_the_ID(),'service_icon_image', true ); ?>"></i> </a>
						<?php } else { ?>
						<i class="fa <?php echo get_post_meta( get_the_ID(),'service_icon_image', true ); ?>"></i>
						<?php } ?>
						</div>
					<?php }
					} ?>
					<?php if($meta_service_link){ ?>
						<h2><a href="<?php echo $meta_service_link;  ?>" <?php if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo 'target="_blank"'; } ?>><?php the_title(); ?></a></h2>
					<?php } else { ?>
						<h2><?php the_title(); ?></h2>
					<?php } ?>
				<p><?php echo get_post_meta( get_the_ID(), 'service_description_text', true ); ?></p>
			</div>
			</div>
		<?php if($i%4==0)
			{	echo "<div class='clearfix'></div>"; 	}
			$i++; endwhile;
		} else { ?>
		
			<div class="col-md-3 col-sm-6">
				<div class="service_area">
					<div class="service_box">
						<i class="fa fa-mobile service_icon_green"></i>
					</div>
					<h2><?php _e('Responsive design','corpbiz'); ?></h2>
					<p><?php echo 'Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget ...'; ?></p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="service_area">
					<div class="service_box">
						<i class="fa fa-mobile service_icon_red"></i>
					</div>
					<h2><?php _e('Responsive design','corpbiz'); ?></h2>
					<p><?php echo 'Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget ...'; ?></p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="service_area">
					<div class="service_box">
						<i class="fa fa-mobile service_icon_blue"></i>
					</div>
					<h2><?php _e('Responsive design','corpbiz'); ?></h2>
					<p><?php echo 'Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget ...'; ?></p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="service_area">
					<div class="service_box">
						<i class="fa fa-mobile service_icon_orange"></i>
					</div>
					<h2><?php _e('Responsive design','corpbiz'); ?></h2>
					<p><?php echo 'Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget ...'; ?></p>
				</div>
			</div>

		
		<?php } ?>		
	</div>	
</div>
</div>
<!--/Homepage Service Section-->