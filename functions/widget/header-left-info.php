<?php
add_action( 'widgets_init', 'wdl_header_left_info_widget' );
function wdl_header_left_info_widget() {
	register_widget( 'wdl_header_left_info_widget' );
}


class wdl_header_left_info_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
			'wdl_header_left_info_widget', // Base ID
			__('WBR : Header Left Info Widget','corpbiz'), // Widget Name
			array(
				'classname' => 'wdl_header_left_info_widget',
				'description' => __('Top header left info widget area','corpbiz'),
			),
			array(
				'width' => 600,
			)
		);
		
	 }
	
	public function widget( $args, $instance ) { 
		
		echo $args['before_widget'];
		
			if($instance['title'])
				echo $args['before_title'] . $instance['title'] . $args['after_title'];
			
			echo '<ul class="header_contact_info">';
			
			if($instance['info_text1']){
				
				echo '<li><i class="fa '. $instance['info_icon1'] .'"></i>'. $instance['info_text1'] .'</li>';
				
			}
			
			if($instance['info_text2']){
				
				echo '<li><i class="fa '. $instance['info_icon2'] .'"></i>'. $instance['info_text2'] .'</li>';
				
			}
			
			echo '</ul>';
		
		
		echo $args['after_widget'];
		
	}
	         
	public function form( $instance ) {
		
		$instance['title'] = ( isset( $instance['title'] ) ) ? $instance['title'] : '';
		$instance['info_icon1'] = ( isset( $instance['info_icon1'] ) ) ? $instance['info_icon1'] : 'fa-phone';
		$instance['info_text1'] = ( isset( $instance['info_text1'] ) ) ? $instance['info_text1'] : '+00386 40 000 111';
		$instance['info_icon2'] = ( isset( $instance['info_icon2'] ) ) ? $instance['info_icon2'] : 'fa-envelope';
		$instance['info_text2'] = ( isset( $instance['info_text2'] ) ) ? $instance['info_text2'] : 'info@corpbiz.com';
		
	?>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'info_icon1' ); ?>"><?php _e( 'Icon','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'info_icon1' ); ?>" name="<?php echo $this->get_field_name( 'info_icon1' ); ?>" type="text" value="<?php echo esc_attr( $instance['info_icon1'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'info_text1' ); ?>"><?php _e( 'InfoText','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'info_text1' ); ?>" name="<?php echo $this->get_field_name( 'info_text1' ); ?>" type="text" value="<?php echo esc_attr( $instance['info_text1'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'info_icon2' ); ?>"><?php _e( 'Icon','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'info_icon2' ); ?>" name="<?php echo $this->get_field_name( 'info_icon2' ); ?>" type="text" value="<?php echo esc_attr( $instance['info_icon2'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'info_text2' ); ?>"><?php _e( 'InfoText','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'info_text2' ); ?>" name="<?php echo $this->get_field_name( 'info_text2' ); ?>" type="text" value="<?php echo esc_attr( $instance['info_text2'] ); ?>" />
	</p>
	
	<p>
	<?php _e( 'Find font awesome icon like : fa-phone  ','corpbiz' ); ?> <a href="https://fortawesome.github.io/Font-Awesome/icons/" target="_blank"><?php _e( 'click here','corpbiz' ); ?><a>
	</p>
	
	<?php
    }
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : '';
		$instance['info_icon1'] = ( ! empty( $new_instance['info_icon1'] ) ) ? $new_instance['info_icon1'] : '';
		$instance['info_text1'] = ( ! empty( $new_instance['info_text1'] ) ) ? $new_instance['info_text1'] : '';
		$instance['info_icon2'] = ( ! empty( $new_instance['info_icon2'] ) ) ? $new_instance['info_icon2'] : '';
		$instance['info_text2'] = ( ! empty( $new_instance['info_text2'] ) ) ? $new_instance['info_text2'] : '';
		
		return $instance;
	}
}