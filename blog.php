<?php
/*
Template Name: Blog
*/
get_header();
get_template_part('index', 'banner');
if(!is_home()) { echo '</div>'; }

$corpbiz_options=theme_data_setup(); 
$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), $corpbiz_options );
?>
<section class="site-content">
	<div class="container">
		<div class="row">
			<div class="<?php corpbiz_post_layout_class(); ?>" >
				<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args = array( 'post_type' => 'post','paged'=>$paged);		
					$post_type_data = new WP_Query( $args );
						while($post_type_data->have_posts()):
						$post_type_data->the_post();
						global $more;
						$more = 0; ?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
					<?php if(has_post_thumbnail()): ?>
					<a class="post-thumbnail" href="<?php the_permalink(); ?>">
					<?php $defalt_arg =array('class' => "img-responsive"); ?>
					<?php the_post_thumbnail('', $defalt_arg); ?>					
					<?php endif; ?>
					</a>
					<div class="entry-header">
						<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					</div>	
					
					<?php 
					// blog meta section
					if( $current_options['blog_meta_section_settings'] == true ) { ?>
					<div class="entry-meta">
						<span class="entry-date">
						<a href="<?php echo get_month_link(get_post_time('Y'),get_post_time('m')); ?>"><?php echo get_the_date('M j, Y'); ?> </a>
						</span>
						<span class="author">
						<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php _e('Posted by', 'corpbiz'); ?> <?php the_author(); ?> </a>
						</span>
						<span class="comments-link">
						<a href="<?php comments_link(); ?>"><?php comments_number(__( 'No Comments', 'corpbiz'), __('One comments', 'corpbiz'), __('% comments', 'corpbiz')); ?></a>
						</span>
						<?php 	$tag_list = get_the_tag_list();
								if(!empty($tag_list)) { ?>
						<span class="tag-links">		
						<?php the_tags('', ',', '<br />'); ?>
						</span>
						<?php } ?>						
					</div>
					<?php } ?>

					<div class="entry-content">
						<p><?php echo get_home_blog_excerpt(); ?></p>				
					</div>	
				</article>
				<?php endwhile; ?>
				<div class="paginations">
				<?php 				
						$Webriti_pagination = new Webriti_pagination();
						$Webriti_pagination->Webriti_page($paged, $post_type_data);					
				?>
				</div>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>	
<?php get_footer(); ?>