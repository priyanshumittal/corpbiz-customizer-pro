<?php 
// Adding customizer home page setting
function corpbiz_style_customizer( $wp_customize ){
//Theme color
class WP_color_Customize_Control extends WP_Customize_Control {
public $type = 'new_menu';

       function render_content()
       
	   {
	   echo '<h3>'.__('Predefined colors', 'corpbiz').'</h3>';
		  $name = '_customize-color-radio-' . $this->id; 
		  foreach($this->choices as $key => $value ) {
            ?>
               <label>
				<input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr( $name ); ?>" data-customize-setting-link="<?php echo esc_attr( $this->id ); ?>" <?php if($this->value() == $key){ echo 'checked'; } ?>>
				<img <?php if($this->value() == $key){ echo 'class="selected_img"'; } ?> src="<?php echo get_template_directory_uri(); ?>/images/color/<?php echo $value; ?>" alt="<?php echo esc_attr( $value ); ?>" />
				</label>
				
            <?php
		  } ?>
		
		  <script>
			jQuery(document).ready(function($) {
				$("#customize-control-corpbiz_options-theme_color label img").click(function(){
					$("#customize-control-corpbiz_options-theme_color label img").removeClass("selected_img");
					$(this).addClass("selected_img");
				});
			});
		  </script>
		  <?php 
       }

}
	/* Theme Style settings */
	$wp_customize->add_section( 'theme_style' , array(
		'title'      => __('Theme style setting', 'corpbiz'),
		'priority'   => 200,
   	) );
	
	 //Theme Color Scheme
	$wp_customize->add_setting(
	'corpbiz_options[theme_color]', array(
	'default' => 'default.css',  
	'capability'     => 'edit_theme_options',
	'type' => 'option',
    ));
	$wp_customize->add_control(new WP_color_Customize_Control($wp_customize,'corpbiz_options[theme_color]',
	array(
        'label'   => __('Predefined colors', 'corpbiz'),
        'section' => 'theme_style',
		'type' => 'radio',
		'settings' => 'corpbiz_options[theme_color]',	
		'choices' => array(
			'default.css' => 'default.png',
            'endeavour.css' => 'endeavour.png',
            'red.css' => 'red.png',
			'green.css' => 'green.png',
			'aqua.css' => 'aqua.png',
			'curious.css' =>'curious.png',
			'orange.css' => 'orange.png',
			
    )
	
	)));
	
	
	$wp_customize->add_setting(
    'corpbiz_options[link_color_enable]',
    array(
        'default' => false,
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    )	
	);
	$wp_customize->add_control(
    'corpbiz_options[link_color_enable]',
    array(
        'label' => __('Skin color enable','corpbiz'),
        'section' => 'theme_style',
        'type' => 'checkbox',
    )
	);
	
	$wp_customize->add_setting(
	'corpbiz_options[link_color]', array(
	'capability'     => 'edit_theme_options',
	'default' => '#66d1b9',
	'type' => 'option',
    ));
	
	$wp_customize->add_control( 
	new WP_Customize_Color_Control( 
	$wp_customize, 
	'corpbiz_options[link_color]', 
	array(
		'label'      => __( 'Skin color', 'corpbiz' ),
		'section'    => 'theme_style',
		'settings'   => 'corpbiz_options[link_color]',
	) ) );
	
	
}
add_action( 'customize_register', 'corpbiz_style_customizer');