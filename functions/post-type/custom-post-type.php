<?php
$current_options = get_option('corpbiz_options');
/************* Home Service custom post type ***********************/	
function corpbiz_service_type()
{	register_post_type( 'corpbiz_service',
		array(
			'labels' => array(
			'name' => __('Service', 'corpbiz'),
			'add_new' => __('Add New', 'corpbiz'),
			'add_new_item' => __('Add New Service','corpbiz'),
			'edit_item' => __('Add New','corpbiz'),
			'new_item' => __('New Link','corpbiz'),
			'all_items' => __('All Services','corpbiz'),
			'view_item' => __('View Link','corpbiz'),
			'search_items' => __('Search Links','corpbiz'),
			'not_found' =>  __('No Links found','corpbiz'),
			'not_found_in_trash' => __('No Links found in Trash','corpbiz'), 
			),
		'supports' => array('title','thumbnail'),
		'show_in' => true,
		'show_in_nav_menus' => false,
		'public' => true,
		'menu_position' =>20,
		'public' => true,
		'menu_icon' => WEBRITI_TEMPLATE_DIR_URI . '/images/option-icon-general.png',
		)
	);
}
add_action( 'init', 'corpbiz_service_type' );
//************* Home project custom post type ***********************
function corpbiz_project_type()
{	register_post_type( 'corpbiz_project',
		array(
			'labels' => array(
				'name' => __('Project Slider', 'corpbiz'),
				'add_new' => __('Add New', 'corpbiz'),
				'add_new_item' => __('Add New Project Slide','corpbiz'),
				'edit_item' => __('Add New','corpbiz'),
				'new_item' => __('New Link','corpbiz'),
				'all_items' => __('All Project Slider','corpbiz'),
				'view_item' => __('View Link','corpbiz'),
				'search_items' => __('Search Links','corpbiz'),
				'not_found' =>  __('No Links found','corpbiz'),
				'not_found_in_trash' => __('No Links found in Trash','corpbiz'), 
			),
			'supports' => array('title','thumbnail'),
			'show_in' => true,
			'show_in_nav_menus' => false,			
			'public' => true,
			'menu_position' =>20,
			'public' => true,
			'menu_icon' => WEBRITI_TEMPLATE_DIR_URI . '/images/slides.png',
		)
	);
}
add_action( 'init', 'corpbiz_project_type' );

//************* Home project custom post type ***********************
function corpbiz_portfolio_type()
{	register_post_type( 'corpbiz_portfolio',
		array(
			'labels' => array(
				'name' => __('Portfolio', 'corpbiz'),
				'add_new' => __('Add New', 'corpbiz'),
				'add_new_item' => __('Add New Portfolio','corpbiz'),
				'edit_item' => __('Add New','corpbiz'),
				'new_item' => __('New Link','corpbiz'),
				'all_items' => __('All Portfolio','corpbiz'),
				'view_item' => __('View Link','corpbiz'),
				'search_items' => __('Search Links','corpbiz'),
				'not_found' =>  __('No Links found','corpbiz'),
				'not_found_in_trash' => __('No Links found in Trash','corpbiz'), 
			),
			'supports' => array('title','thumbnail'),
			'show_in' => true,
			'show_in_nav_menus' => false,			
			'public' => true,
			'menu_position' =>20,
			'public' => true,
			'menu_icon' => WEBRITI_TEMPLATE_DIR_URI . '/images/option-icon-media.png',
		)
	);
}
add_action( 'init', 'corpbiz_portfolio_type' );



//Testimonial
function corpbiz_testimonial() {
	register_post_type( 'corpbiz_testimonial',
		array(
			'labels' => array(
			'name' => __('Clients', 'corpbiz'),
			'add_new' => __('Add New', 'corpbiz'),
			'add_new_item' => __('Add New Client','corpbiz'),
			'edit_item' => __('Add New','corpbiz'),
			'new_item' => __('New Link','corpbiz'),
			'all_items' => __('All Clients','corpbiz'),
			'view_item' => __('View Client','corpbiz'),
			'search_items' => __('Search Client','corpbiz'),
			'not_found' =>  __('No Client found','corpbiz'),
			'not_found_in_trash' => __('No Client found in Trash','corpbiz'), 
			),
		'supports' => array('title','thumbnail'),
		'show_in' => true,
		'show_in_nav_menus' => false,		
		'public' => true,
		'menu_position' =>20,
		'public' => true,		
		)
	);
}
add_action( 'init', 'corpbiz_testimonial' );

function corpbiz_team_type()
{	register_post_type( 'corpbiz_team',
		array(
			'labels' => array(
				'name' => __('Our Team', 'corpbiz'),
				'add_new' => __('Add New', 'corpbiz'),
                'add_new_item' => __('Add New Team','corpbiz'),
				'edit_item' => __('Add New','corpbiz'),
				'new_item' => __('New Link','corpbiz'),
				'all_items' => __('All Team','corpbiz'),
				'view_item' => __('View Link','corpbiz'),
				'search_items' => __('Search Links','corpbiz'),
				'not_found' =>  __('No Links found','corpbiz'),
				'not_found_in_trash' => __('No Links found in Trash','corpbiz'), 
				),
			'supports' => array('title','thumbnail'),
			'show_in' => true,
			'show_in_nav_menus' => false,			
			'public' => true,
			'menu_position' => 20,
			'public' => true,			
			)
	);
}
add_action( 'init', 'corpbiz_team_type' );
?>