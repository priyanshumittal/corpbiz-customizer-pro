<?php
function corpbiz_slider_customizer( $wp_customize ) {


//slider Section 
	$wp_customize->add_panel( 'corpbiz_slider_setting', array(
		'priority'       => 400,
		'capability'     => 'edit_theme_options',
		'title'      => __('Slider settings', 'corpbiz'),
	) );
	
	
	/* slider settings */
	$wp_customize->add_section( 'slider_settings' , array(
		'title'      => __('Slider settings', 'corpbiz'),
		'panel'  => 'corpbiz_slider_setting',
		'priority'   => 1,
   	) );
	
	// enable / disable home banner
	$wp_customize->add_setting(
	'corpbiz_options[home_banner_enabled]', array(
        'default'        => true,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[home_banner_enabled]', array(
        'label'   => __('Enable home banner', 'corpbiz'),
        'section' => 'slider_settings',
        'type'    => 'checkbox',
		'priority'   => 1,
    )); 
	
	
	$wp_customize->add_setting(
	'corpbiz_options[animation]', array(
        'default'        => 'slide',
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[animation]', array(
        'label'   => __('Animation', 'corpbiz'),
        'section' => 'slider_settings',
        'type'    => 'select',
		'choices'=>array('slide'=> __('slide','corpbiz'),'fade'=>__('fade','corpbiz')),
		'priority'   => 4,
    )); // slider animation
	
	
	$wp_customize->add_setting(
	'corpbiz_options[animationSpeed]', array(
        'default'        => 2000,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[animationSpeed]', array(
        'label'   => __('Animation speed', 'corpbiz'),
        'section' => 'slider_settings',
        'type'    => 'select',
		'choices'=>array(500=>0.5,1000=>1.0,1500=>1.5,2000=>2.0,2500=>2.5,3000=>3.0,3500=>3.5,4000=>4.0,4500=>4.5,5000=>5.0,5500=>5.5),
		'priority'   => 5,
    )); // slider animation speed
	
	$wp_customize->add_setting(
	'corpbiz_options[slideshowSpeed]', array(
        'default'        => 2500,
        'capability'     => 'edit_theme_options',
		'type' => 'option',
    ));
	$wp_customize->add_control('corpbiz_options[slideshowSpeed]', array(
        'label'   => __('Slideshow speed', 'corpbiz'),
        'section' => 'slider_settings',
        'type'    => 'select',
		'choices'=>array(500=>0.5,1000=>1.0,1500=>1.5,2000=>2.0,2500=>2.5,3000=>3.0,3500=>3.5,4000=>4.0,4500=>4.5,5000=>5.0,5500=>5.5),
		'priority'   => 6,
    )); // slider Slideshow speed
	
	//Slider One
	$wp_customize->add_section(
        'image_slider_one',
        array(
            'title' => __('Image One','corpbiz'),
            'description' => '',
			'panel'  => 'corpbiz_slider_setting',)
    );
	
	
	$wp_customize->add_setting( 'corpbiz_options[slider_image_one]',array('default' => get_template_directory_uri().'/images/slides/slide01.jpg','sanitize_callback' => 'esc_url_raw','type' => 'option',
	));
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'corpbiz_options[slider_image_one]',
			array(
				'type'        => 'upload',
				'label' => __('Image','corpbiz'),
				'section' => 'example_section_one',
				'settings' =>'corpbiz_options[slider_image_one]',
				'section' => 'image_slider_one',
				
			)
		)
	);
	
	//Slider Title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_one_title]', array(
        'default'        => __('Built your website','corpbiz'),
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_one_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'image_slider_one',
		'type' => 'text',
    ));
	
	//Slider sub title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_one_description]', array(
        'default'        => __('Corpbiz is extremely clean in each code line, intensely awesome in very homepage, supper easy to use, highly flexible with responsive feature.','corpbiz'),
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_one_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'image_slider_one',
		'type' => 'textarea',
    ));
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_one_readmore_text]',
	array( 
	'default' => __('Read More','corpbiz'),
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) 
	);

	$wp_customize->add_control (
	'corpbiz_options[slider_one_readmore_text]',
	array (  
	'label' => __('Button Text','corpbiz'),
	'section' => 'image_slider_one',
	'type' => 'text',
	) );
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_one_readmore_link]',
	array( 
	'default' => '#',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) );

	$wp_customize->add_control (
	'corpbiz_options[slider_one_readmore_link]',
	array (  
	'label' => __('Button Link','corpbiz'),
	'section' => 'image_slider_one',
	'type' => 'text',
	) );

	$wp_customize->add_setting(
		'corpbiz_options[slider_one_readmore_ink_target]',
		array('capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'default' => true,
		'type' => 'option',		
		));

	$wp_customize->add_control(
		'corpbiz_options[slider_one_readmore_ink_target]',
		array(
			'type' => 'checkbox',
			'label' => __('Open link in new tab','corpbiz'),
			'section' => 'image_slider_one',
		)
	);
	
	
	
	//Slider two
	$wp_customize->add_section(
        'image_slider_two',
        array(
            'title' => __('Image Two','corpbiz'),
            'description' => '',
			'panel'  => 'corpbiz_slider_setting',)
    );
	
	
	$wp_customize->add_setting( 'corpbiz_options[slider_image_two]',array('default' => get_template_directory_uri().'/images/slides/slide02.jpg','sanitize_callback' => 'esc_url_raw','type' => 'option',	
	));
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'corpbiz_options[slider_image_two]',
			array(
				'type'        => 'upload',
				'label' => __('Image','corpbiz'),
				'settings' =>'corpbiz_options[slider_image_two]',
				'section' => 'image_slider_two',
				
			)
		)
	);
	
	//Slider Title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_two_title]', array(
        'default'        => __('Powerful Bootstrap theme','corpbiz'),
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text','type' => 'option',	
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_two_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'image_slider_two',
		'type' => 'text',
    ));
	
	//Slider sub title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_two_description]', array(
       'default'        => 'Sea summo mazim ex, ea errem eleifend definitionem vim. Ut nec hinc dolor possim mei ludus efficiendi ei sea summo mazim ex',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text','type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_two_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'image_slider_two',
		'type' => 'textarea',
    ));
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_two_readmore_text]',
	array( 
	'default' => __('Read More','corpbiz'),
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',	
	) 
	);

	$wp_customize->add_control (
	'corpbiz_options[slider_two_readmore_text]',
	array (  
	'label' => __('Button Text','corpbiz'),
	'section' => 'image_slider_two',
	'type' => 'text',
	) );
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_two_readmore_link]',
	array( 
	'default' => '#',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',	
	) );

	$wp_customize->add_control (
	'corpbiz_options[slider_two_readmore_link]',
	array (  
	'label' => __('Button Link','corpbiz'),
	'section' => 'image_slider_two',
	'type' => 'text',
	) );

	$wp_customize->add_setting(
		'corpbiz_options[slider_two_readmore_ink_target]',
		array('capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'default' => true,
		'type' => 'option',
		));

	$wp_customize->add_control(
		'corpbiz_options[slider_two_readmore_ink_target]',
		array(
			'type' => 'checkbox',
			'label' => __('Open link in new tab','corpbiz'),
			'section' => 'image_slider_two',
		)
	);
	
	//Slider three
	$wp_customize->add_section(
        'image_slider_three',
        array(
            'title' => __('Image Three','corpbiz'),
            'description' => '',
			'panel'  => 'corpbiz_slider_setting',)
    );
	
	
	$wp_customize->add_setting( 'corpbiz_options[slider_image_three]',array('default' => get_template_directory_uri().'/images/slides/slide03.jpg','sanitize_callback' => 'esc_url_raw','type' => 'option',
	));
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'corpbiz_options[slider_image_three]',
			array(
				'type'        => 'upload',
				'label' => __('Image','corpbiz'),
				'settings' =>'corpbiz_options[slider_image_three]',
				'section' => 'image_slider_three',
				
			)
		)
	);
	
	//Slider Title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_three_title]', array(
        'default'        => __('Simpler landing','corpbiz'),
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_three_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'image_slider_three',
		'type' => 'text',
    ));
	
	//Slider sub title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_three_description]', array(
        'default'        => 'Sea summo mazim ex, ea errem eleifend definitionem vim. Ut nec hinc dolor possim mei ludus efficiendi ei sea summo mazim ex',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_three_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'image_slider_three',
		'type' => 'textarea',
    ));
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_three_readmore_text]',
	array( 
	'default' => __('Read More','corpbiz'),
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) 
	);

	$wp_customize->add_control (
	'corpbiz_options[slider_three_readmore_text]',
	array (  
	'label' => __('Button Text','corpbiz'),
	'section' => 'image_slider_three',
	'type' => 'text',
	) );
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_three_readmore_link]',
	array( 
	'default' => '#',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) );

	$wp_customize->add_control (
	'corpbiz_options[slider_three_readmore_link]',
	array (  
	'label' => __('Button Link','corpbiz'),
	'section' => 'image_slider_three',
	'type' => 'text',
	) );

	$wp_customize->add_setting(
		'corpbiz_options[slider_three_readmore_ink_target]',
		array('capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'default' => true,
		'type' => 'option',
		));

	$wp_customize->add_control(
		'corpbiz_options[slider_three_readmore_ink_target]',
		array(
			'type' => 'checkbox',
			'label' => __('Open link in new tab','corpbiz'),
			'section' => 'image_slider_three',
		)
	);
	
	//Slider Four
	$wp_customize->add_section(
        'image_slider_four',
        array(
            'title' => __('Image Four','corpbiz'),
            'description' => '',
			'panel'  => 'corpbiz_slider_setting',)
    );
	
	
	$wp_customize->add_setting( 'corpbiz_options[slider_image_four]',array('default' => '','sanitize_callback' => 'esc_url_raw','type' => 'option',
	));
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'corpbiz_options[slider_image_four]',
			array(
				'type'        => 'upload',
				'label' => __('Image','corpbiz'),
				'settings' =>'corpbiz_options[slider_image_four]',
				'section' => 'image_slider_four',
				
			)
		)
	);
	
	//Slider Title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_four_title]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_four_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'image_slider_four',
		'type' => 'text',
    ));
	
	//Slider sub title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_four_description]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_four_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'image_slider_four',
		'type' => 'textarea',
    ));
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_four_readmore_text]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) 
	);

	$wp_customize->add_control (
	'corpbiz_options[slider_four_readmore_text]',
	array (  
	'label' => __('Button Text','corpbiz'),
	'section' => 'image_slider_four',
	'type' => 'text',
	) );
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_four_readmore_link]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) );

	$wp_customize->add_control (
	'corpbiz_options[slider_four_readmore_link]',
	array (  
	'label' => __('Button Link','corpbiz'),
	'section' => 'image_slider_four',
	'type' => 'text',
	) );

	$wp_customize->add_setting(
		'corpbiz_options[slider_four_readmore_ink_target]',
		array('capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		));

	$wp_customize->add_control(
		'corpbiz_options[slider_four_readmore_ink_target]',
		array(
			'type' => 'checkbox',
			'label' => __('Open link in new tab','corpbiz'),
			'section' => 'image_slider_four',
		)
	);
	
	
	//Slider Five
	$wp_customize->add_section(
        'image_slider_five',
        array(
            'title' => __('Image Five','corpbiz'),
            'description' => '',
			'panel'  => 'corpbiz_slider_setting',)
    );
	
	
	$wp_customize->add_setting( 'corpbiz_options[slider_image_five]',array('default' => '','sanitize_callback' => 'esc_url_raw','type' => 'option',
	));
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'corpbiz_options[slider_image_five]',
			array(
				'type'        => 'upload',
				'label' => __('Image','corpbiz'),
				'settings' =>'corpbiz_options[slider_image_five]',
				'section' => 'image_slider_five',
				
			)
		)
	);
	
	//Slider Title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_five_title]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_five_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'image_slider_five',
		'type' => 'text',
    ));
	
	//Slider sub title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_five_description]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_five_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'image_slider_five',
		'type' => 'textarea',
    ));
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_five_readmore_text]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) 
	);

	$wp_customize->add_control (
	'corpbiz_options[slider_five_readmore_text]',
	array (  
	'label' => __('Button Text','corpbiz'),
	'section' => 'image_slider_five',
	'type' => 'text',
	) );
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_five_readmore_link]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) );

	$wp_customize->add_control (
	'corpbiz_options[slider_five_readmore_link]',
	array (  
	'label' => __('Button Link','corpbiz'),
	'section' => 'image_slider_five',
	'type' => 'text',
	) );

	$wp_customize->add_setting(
		'corpbiz_options[slider_five_readmore_ink_target]',
		array('capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		));

	$wp_customize->add_control(
		'corpbiz_options[slider_five_readmore_ink_target]',
		array(
			'type' => 'checkbox',
			'label' => __('Open link in new tab','corpbiz'),
			'section' => 'image_slider_five',
		)
	);
	
	//Slider six
	$wp_customize->add_section(
        'image_slider_six',
        array(
            'title' => __('Image Six','corpbiz'),
            'description' => '',
			'panel'  => 'corpbiz_slider_setting',)
    );
	
	
	$wp_customize->add_setting( 'corpbiz_options[slider_image_six]',array('default' => '','sanitize_callback' => 'esc_url_raw','type' => 'option',
	));
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'corpbiz_options[slider_image_six]',
			array(
				'type'        => 'upload',
				'label' => __('Image','corpbiz'),
				'settings' =>'corpbiz_options[slider_image_six]',
				'section' => 'image_slider_six',
				
			)
		)
	);
	
	//Slider Title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_six_title]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_six_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'image_slider_six',
		'type' => 'text',
    ));
	
	//Slider sub title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_six_description]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_six_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'image_slider_six',
		'type' => 'textarea',
    ));
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_six_readmore_text]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) 
	);

	$wp_customize->add_control (
	'corpbiz_options[slider_six_readmore_text]',
	array (  
	'label' => __('Button Text','corpbiz'),
	'section' => 'image_slider_six',
	'type' => 'text',
	) );
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_six_readmore_link]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) );

	$wp_customize->add_control (
	'corpbiz_options[slider_six_readmore_link]',
	array (  
	'label' => __('Button Link','corpbiz'),
	'section' => 'image_slider_six',
	'type' => 'text',
	) );

	$wp_customize->add_setting(
		'corpbiz_options[slider_six_readmore_ink_target]',
		array('capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		));

	$wp_customize->add_control(
		'corpbiz_options[slider_six_readmore_ink_target]',
		array(
			'type' => 'checkbox',
			'label' => __('Open link in new tab','corpbiz'),
			'section' => 'image_slider_six',
		)
	);
	
	//Slider seven
	$wp_customize->add_section(
        'image_slider_seven',
        array(
            'title' => __('Image Seven','corpbiz'),
            'description' => '',
			'panel'  => 'corpbiz_slider_setting',)
    );
	
	
	$wp_customize->add_setting( 'corpbiz_options[slider_image_seven]',array('default' => '','sanitize_callback' => 'esc_url_raw','type' => 'option',
	));
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'corpbiz_options[slider_image_seven]',
			array(
				'type'        => 'upload',
				'label' => __('Image','corpbiz'),
				'settings' =>'corpbiz_options[slider_image_seven]',
				'section' => 'image_slider_seven',
				
			)
		)
	);
	
	//Slider Title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_seven_title]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_seven_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'image_slider_seven',
		'type' => 'text',
    ));
	
	//Slider sub title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_seven_description]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
		  ));
    $wp_customize->add_control('corpbiz_options[slider_image_seven_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'image_slider_seven',
		'type' => 'textarea',
    ));
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_seven_readmore_text]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) 
	);

	$wp_customize->add_control (
	'corpbiz_options[slider_seven_readmore_text]',
	array (  
	'label' => __('Button Text','corpbiz'),
	'section' => 'image_slider_seven',
	'type' => 'text',
	) );
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_seven_readmore_link]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) );

	$wp_customize->add_control (
	'corpbiz_options[slider_seven_readmore_link]',
	array (  
	'label' => __('Button Link','corpbiz'),
	'section' => 'image_slider_seven',
	'type' => 'text',
	) );

	$wp_customize->add_setting(
		'corpbiz_options[slider_seven_readmore_ink_target]',
		array('capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'default' => '',
		'type' => 'option',
		));

	$wp_customize->add_control(
		'corpbiz_options[slider_seven_readmore_ink_target]',
		array(
			'type' => 'checkbox',
			'label' => __('Open link in new tab','corpbiz'),
			'section' => 'image_slider_seven',
		)
	);
	
	//Slider Eight
	$wp_customize->add_section(
        'image_slider_eight',
        array(
            'title' => __('Image Eight','corpbiz'),
            'description' => '',
			'panel'  => 'corpbiz_slider_setting',)
    );
	
	$wp_customize->add_setting( 'corpbiz_options[slider_image_eight]',array('default' => '','sanitize_callback' => 'esc_url_raw','type' => 'option',
	));
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'corpbiz_options[slider_image_eight]',
			array(
				'type'        => 'upload',
				'label' => __('Image','corpbiz'),
				'settings' =>'corpbiz_options[slider_image_eight]',
				'section' => 'image_slider_eight',
				
			)
		)
	);
	
	//Slider Title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_eight_title]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_eight_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'image_slider_eight',
		'type' => 'text',
    ));
	
	//Slider sub title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_eight_description]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
		  ));
    $wp_customize->add_control('corpbiz_options[slider_image_eight_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'image_slider_eight',
		'type' => 'textarea',
    ));
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_eight_readmore_text]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) 
	);

	$wp_customize->add_control (
	'corpbiz_options[slider_eight_readmore_text]',
	array (  
	'label' => __('Button Text','corpbiz'),
	'section' => 'image_slider_eight',
	'type' => 'text',
	) );
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_eight_readmore_link]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) );

	$wp_customize->add_control (
	'corpbiz_options[slider_eight_readmore_link]',
	array (  
	'label' => __('Button Link','corpbiz'),
	'section' => 'image_slider_eight',
	'type' => 'text',
	) );

	$wp_customize->add_setting(
		'corpbiz_options[slider_eight_readmore_ink_target]',
		array('capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		'default' => '',
		));

	$wp_customize->add_control(
		'corpbiz_options[slider_eight_readmore_ink_target]',
		array(
			'type' => 'checkbox',
			'label' => __('Open link in new tab','corpbiz'),
			'section' => 'image_slider_eight',
		)
	);
	
	//Slider Nine
	$wp_customize->add_section(
        'image_slider_nine',
        array(
            'title' => __('Image Nine','corpbiz'),
            'description' => '',
			'panel'  => 'corpbiz_slider_setting',)
    );
	
	
	$wp_customize->add_setting( 'corpbiz_options[slider_image_nine]',array('default' => '','sanitize_callback' => 'esc_url_raw','type' => 'option',
	));
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'corpbiz_options[slider_image_nine]',
			array(
				'type'        => 'upload',
				'label' => __('Image','corpbiz'),
				'settings' =>'corpbiz_options[slider_image_nine]',
				'section' => 'image_slider_nine',
				
			)
		)
	);
	
	//Slider Title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_nine_title]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_nine_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'image_slider_nine',
		'type' => 'text',
    ));
	
	//Slider sub title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_nine_description]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
		  ));
    $wp_customize->add_control('corpbiz_options[slider_image_nine_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'image_slider_nine',
		'type' => 'textarea',
    ));
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_nine_readmore_text]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) 
	);

	$wp_customize->add_control (
	'corpbiz_options[slider_nine_readmore_text]',
	array (  
	'label' => __('Button Text','corpbiz'),
	'section' => 'image_slider_nine',
	'type' => 'text',
	) );
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_nine_readmore_link]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) );

	$wp_customize->add_control (
	'corpbiz_options[slider_nine_readmore_link]',
	array (  
	'label' => __('Button Link','corpbiz'),
	'section' => 'image_slider_nine',
	'type' => 'text',
	) );

	$wp_customize->add_setting(
		'corpbiz_options[slider_nine_readmore_ink_target]',
		array('capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'default' => '',
		'type' => 'option',
		));

	$wp_customize->add_control(
		'corpbiz_options[slider_nine_readmore_ink_target]',
		array(
			'type' => 'checkbox',
			'label' => __('Open link in new tab','corpbiz'),
			'section' => 'image_slider_nine',
		)
	);
	
	//Slider Ten
	$wp_customize->add_section(
        'image_slider_ten',
        array(
            'title' => __('Image Ten','corpbiz'),
            'description' => '',
			'panel'  => 'corpbiz_slider_setting',)
    );
	
	
	$wp_customize->add_setting( 'corpbiz_options[slider_image_ten]',array('default' => '','sanitize_callback' => 'esc_url_raw','type' => 'option',
	));
 
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'corpbiz_options[slider_image_ten]',
			array(
				'type'        => 'upload',
				'label' => __('Image','corpbiz'),
				'settings' =>'corpbiz_options[slider_image_ten]',
				'section' => 'image_slider_ten',
				
			)
		)
	);
	
	//Slider Title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_ten_title]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
    ));
    $wp_customize->add_control('corpbiz_options[slider_image_ten_title]', array(
        'label'   => __('Title', 'corpbiz'),
        'section' => 'image_slider_ten',
		'type' => 'text',
    ));
	
	//Slider sub title
	$wp_customize->add_setting(
	'corpbiz_options[slider_image_ten_description]', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'corpbiz_input_field_sanitize_text',
		'type' => 'option',
		  ));
    $wp_customize->add_control('corpbiz_options[slider_image_ten_description]', array(
        'label'   => __('Description', 'corpbiz'),
        'section' => 'image_slider_ten',
		'type' => 'textarea',
    ));
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_ten_readmore_text]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) 
	);

	$wp_customize->add_control (
	'corpbiz_options[slider_ten_readmore_text]',
	array (  
	'label' => __('Button Text','corpbiz'),
	'section' => 'image_slider_ten',
	'type' => 'text',
	) );
	
	$wp_customize ->add_setting (
	'corpbiz_options[slider_ten_readmore_link]',
	array( 
	'default' => '',
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
	) );

	$wp_customize->add_control (
	'corpbiz_options[slider_ten_readmore_link]',
	array (  
	'label' => __('Button Link','corpbiz'),
	'section' => 'image_slider_ten',
	'type' => 'text',
	) );

	$wp_customize->add_setting(
		'corpbiz_options[slider_ten_readmore_ink_target]',
		array('capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'default' => '',
		'type' => 'option',
		));

	$wp_customize->add_control(
		'corpbiz_options[slider_ten_readmore_ink_target]',
		array(
			'type' => 'checkbox',
			'label' => __('Open link in new tab','corpbiz'),
			'section' => 'image_slider_ten',
		)
	);
	
	
	function corpbiz_input_field_sanitize_text( $input ) 
	{
	return wp_kses_post( force_balance_tags( $input ) );
	}
	function corpbiz_input_field_sanitize_html( $input ) 
	{
	return force_balance_tags( $input );
	}
	}
	add_action( 'customize_register', 'corpbiz_slider_customizer' );
	?>