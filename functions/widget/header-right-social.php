<?php
add_action( 'widgets_init', 'wdl_header_right_social_widget' );
function wdl_header_right_social_widget() {
	register_widget( 'wdl_header_right_social_widget' );
}


class wdl_header_right_social_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
			'wdl_header_right_social_widget', // Base ID
			__('WBR : Header Right Social Widget','corpbiz'), // Widget Name
			array(
				'classname' => 'wdl_header_right_social_widget',
				'description' => __('Top header right social widget area','corpbiz'),
			),
			array(
				'width' => 600,
			)
		);
		
	 }
	
	public function widget( $args, $instance ) { 
		
		echo $args['before_widget'];
		
			if($instance['title'])
				echo $args['before_title'] . $instance['title'] . $args['after_title'];
			
			echo '<ul class="header_contact_social">';
			
			if($instance['social_url1'])
				echo '<li><a href="'. $instance['social_url1'] .'" '. ( $instance['window_open'] == true ? 'target="_blank"' : '' ) .'><i class="fa '. $instance['social_icon1'] .'"></i></a></li>';
			
			if($instance['social_url2'])
				echo '<li><a href="'. $instance['social_url2'] .'" '. ( $instance['window_open'] == true ? 'target="_blank"' : '' ) .'><i class="fa '. $instance['social_icon2'] .'"></i></a></li>';
			
			if($instance['social_url3'])
				echo '<li><a href="'. $instance['social_url3'] .'" '. ( $instance['window_open'] == true ? 'target="_blank"' : '' ) .'><i class="fa '. $instance['social_icon3'] .'"></i></a></li>';
			
			if($instance['social_url4'])
				echo '<li><a href="'. $instance['social_url4'] .'" '. ( $instance['window_open'] == true ? 'target="_blank"' : '' ) .'><i class="fa '. $instance['social_icon4'] .'"></i></a></li>';
			
			if($instance['social_url5'])
				echo '<li><a href="'. $instance['social_url5'] .'" '. ( $instance['window_open'] == true ? 'target="_blank"' : '' ) .'><i class="fa '. $instance['social_icon5'] .'"></i></a></li>';
			
			if($instance['social_url6'])
				echo '<li><a href="'. $instance['social_url6'] .'" '. ( $instance['window_open'] == true ? 'target="_blank"' : '' ) .'><i class="fa '. $instance['social_icon6'] .'"></i></a></li>';
			
			if($instance['social_url7'])
				echo '<li><a href="'. $instance['social_url7'] .'" '. ( $instance['window_open'] == true ? 'target="_blank"' : '' ) .'><i class="fa '. $instance['social_icon7'] .'"></i></a></li>';
			
			if($instance['social_url8'])
				echo '<li><a href="'. $instance['social_url8'] .'" '. ( $instance['window_open'] == true ? 'target="_blank"' : '' ) .'><i class="fa '. $instance['social_icon8'] .'"></i></a></li>';
			
			if($instance['social_url9'])
				echo '<li><a href="'. $instance['social_url9'] .'" '. ( $instance['window_open'] == true ? 'target="_blank"' : '' ) .'><i class="fa '. $instance['social_icon9'] .'"></i></a></li>';
			
			if($instance['social_url10'])
				echo '<li><a href="'. $instance['social_url10'] .'" '. ( $instance['window_open'] == true ? 'target="_blank"' : '' ) .'><i class="fa '. $instance['social_icon10'] .'"></i></a></li>';
			
			echo '</ul>';
		
		
		echo $args['after_widget'];
		
	}
	         
	public function form( $instance ) {
		
		$instance['title'] = ( isset( $instance['title'] ) ) ? $instance['title'] : '';
		
		$instance['window_open'] = ( isset( $instance['window_open'] ) ) ? $instance['window_open'] : false ;
		
		$instance['social_icon1'] = ( isset( $instance['social_icon1'] ) ) ? $instance['social_icon1'] : 'fa-facebook';
		$instance['social_url1'] = ( isset( $instance['social_url1'] ) ) ? $instance['social_url1'] : '#';
		
		$instance['social_icon2'] = ( isset( $instance['social_icon2'] ) ) ? $instance['social_icon2'] : 'fa-twitter';
		$instance['social_url2'] = ( isset( $instance['social_url2'] ) ) ? $instance['social_url2'] : '#';
		
		$instance['social_icon3'] = ( isset( $instance['social_icon3'] ) ) ? $instance['social_icon3'] : 'fa-linkedin';
		$instance['social_url3'] = ( isset( $instance['social_url3'] ) ) ? $instance['social_url3'] : '#';
		
		$instance['social_icon4'] = ( isset( $instance['social_icon4'] ) ) ? $instance['social_icon4'] : 'fa-google-plus';
		$instance['social_url4'] = ( isset( $instance['social_url4'] ) ) ? $instance['social_url4'] : '#';
		
		$instance['social_icon5'] = ( isset( $instance['social_icon5'] ) ) ? $instance['social_icon5'] : 'fa-dribbble';
		$instance['social_url5'] = ( isset( $instance['social_url5'] ) ) ? $instance['social_url5'] : '#';
		
		$instance['social_icon6'] = ( isset( $instance['social_icon6'] ) ) ? $instance['social_icon6'] : 'fa-dropbox';
		$instance['social_url6'] = ( isset( $instance['social_url6'] ) ) ? $instance['social_url6'] : '#';
		
		$instance['social_icon7'] = ( isset( $instance['social_icon7'] ) ) ? $instance['social_icon7'] : 'fa-flickr';
		$instance['social_url7'] = ( isset( $instance['social_url7'] ) ) ? $instance['social_url7'] : '#';
		
		$instance['social_icon8'] = ( isset( $instance['social_icon8'] ) ) ? $instance['social_icon8'] : 'fa-instagram';
		$instance['social_url8'] = ( isset( $instance['social_url8'] ) ) ? $instance['social_url8'] : '#';
		
		$instance['social_icon9'] = ( isset( $instance['social_icon9'] ) ) ? $instance['social_icon9'] : 'fa-tumblr';
		$instance['social_url9'] = ( isset( $instance['social_url9'] ) ) ? $instance['social_url9'] : '#';
		
		$instance['social_icon10'] = ( isset( $instance['social_icon10'] ) ) ? $instance['social_icon10'] : 'fa-vk';
		$instance['social_url10'] = ( isset( $instance['social_url10'] ) ) ? $instance['social_url10'] : '#';
		
	?>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'social_icon1' ); ?>"><?php _e( 'Social Icon','corpbiz' ); ?></label> 
		<select id="<?php echo $this->get_field_id( 'social_icon1' ); ?>" name="<?php echo $this->get_field_name( 'social_icon1' ); ?>">
			<option value="fa-facebook" <?php if($instance['social_icon1']=='fa-facebook') echo 'selected'; ?>><?php _e('Facebook','corpbiz'); ?></option>
			<option value="fa-twitter" <?php if($instance['social_icon1']=='fa-twitter') echo 'selected'; ?>><?php _e('Twitter','corpbiz'); ?></option>
			<option value="fa-linkedin" <?php if($instance['social_icon1']=='fa-linkedin') echo 'selected'; ?>><?php _e('LinkedIn','corpbiz'); ?></option>
			<option value="fa-google-plus" <?php if($instance['social_icon1']=='fa-google-plus') echo 'selected'; ?>><?php _e('GooglePlus','corpbiz'); ?></option>
			<option value="fa-dribbble" <?php if($instance['social_icon1']=='fa-dribbble') echo 'selected'; ?>><?php _e('Dribbble','corpbiz'); ?></option>
			<option value="fa-dropbox" <?php if($instance['social_icon1']=='fa-dropbox') echo 'selected'; ?>><?php _e('Dropbox','corpbiz'); ?></option>
			<option value="fa-flickr" <?php if($instance['social_icon1']=='fa-flickr') echo 'selected'; ?>><?php _e('Flickr','corpbiz'); ?></option>
			<option value="fa-instagram" <?php if($instance['social_icon1']=='fa-instagram') echo 'selected'; ?>><?php _e('Instagram','corpbiz'); ?></option>
			<option value="fa-tumblr" <?php if($instance['social_icon1']=='fa-tumblr') echo 'selected'; ?>><?php _e('Tumblr','corpbiz'); ?></option>
			<option value="fa-vk" <?php if($instance['social_icon1']=='fa-vk') echo 'selected'; ?>><?php _e('Vk','corpbiz'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'social_url1' ); ?>"><?php _e( 'URL','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'social_url1' ); ?>" name="<?php echo $this->get_field_name( 'social_url1' ); ?>" type="text" value="<?php echo esc_attr( $instance['social_url1'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'social_icon2' ); ?>"><?php _e( 'Social Icon','corpbiz' ); ?></label> 
		<select id="<?php echo $this->get_field_id( 'social_icon2' ); ?>" name="<?php echo $this->get_field_name( 'social_icon2' ); ?>">
			<option value="fa-facebook" <?php if($instance['social_icon2']=='fa-facebook') echo 'selected'; ?>><?php _e('Facebook','corpbiz'); ?></option>
			<option value="fa-twitter" <?php if($instance['social_icon2']=='fa-twitter') echo 'selected'; ?>><?php _e('Twitter','corpbiz'); ?></option>
			<option value="fa-linkedin" <?php if($instance['social_icon2']=='fa-linkedin') echo 'selected'; ?>><?php _e('LinkedIn','corpbiz'); ?></option>
			<option value="fa-google-plus" <?php if($instance['social_icon2']=='fa-google-plus') echo 'selected'; ?>><?php _e('GooglePlus','corpbiz'); ?></option>
			<option value="fa-dribbble" <?php if($instance['social_icon2']=='fa-dribbble') echo 'selected'; ?>><?php _e('Dribbble','corpbiz'); ?></option>
			<option value="fa-dropbox" <?php if($instance['social_icon2']=='fa-dropbox') echo 'selected'; ?>><?php _e('Dropbox','corpbiz'); ?></option>
			<option value="fa-flickr" <?php if($instance['social_icon2']=='fa-flickr') echo 'selected'; ?>><?php _e('Flickr','corpbiz'); ?></option>
			<option value="fa-instagram" <?php if($instance['social_icon2']=='fa-instagram') echo 'selected'; ?>><?php _e('Instagram','corpbiz'); ?></option>
			<option value="fa-tumblr" <?php if($instance['social_icon2']=='fa-tumblr') echo 'selected'; ?>><?php _e('Tumblr','corpbiz'); ?></option>
			<option value="fa-vk" <?php if($instance['social_icon2']=='fa-vk') echo 'selected'; ?>><?php _e('Vk','corpbiz'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'social_url2' ); ?>"><?php _e( 'URL','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'social_url2' ); ?>" name="<?php echo $this->get_field_name( 'social_url2' ); ?>" type="text" value="<?php echo esc_attr( $instance['social_url2'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'social_icon3' ); ?>"><?php _e( 'Social Icon','corpbiz' ); ?></label> 
		<select id="<?php echo $this->get_field_id( 'social_icon3' ); ?>" name="<?php echo $this->get_field_name( 'social_icon3' ); ?>">
			<option value="fa-facebook" <?php if($instance['social_icon3']=='fa-facebook') echo 'selected'; ?>><?php _e('Facebook','corpbiz'); ?></option>
			<option value="fa-twitter" <?php if($instance['social_icon3']=='fa-twitter') echo 'selected'; ?>><?php _e('Twitter','corpbiz'); ?></option>
			<option value="fa-linkedin" <?php if($instance['social_icon3']=='fa-linkedin') echo 'selected'; ?>><?php _e('LinkedIn','corpbiz'); ?></option>
			<option value="fa-google-plus" <?php if($instance['social_icon3']=='fa-google-plus') echo 'selected'; ?>><?php _e('GooglePlus','corpbiz'); ?></option>
			<option value="fa-dribbble" <?php if($instance['social_icon3']=='fa-dribbble') echo 'selected'; ?>><?php _e('Dribbble','corpbiz'); ?></option>
			<option value="fa-dropbox" <?php if($instance['social_icon3']=='fa-dropbox') echo 'selected'; ?>><?php _e('Dropbox','corpbiz'); ?></option>
			<option value="fa-flickr" <?php if($instance['social_icon3']=='fa-flickr') echo 'selected'; ?>><?php _e('Flickr','corpbiz'); ?></option>
			<option value="fa-instagram" <?php if($instance['social_icon3']=='fa-instagram') echo 'selected'; ?>><?php _e('Instagram','corpbiz'); ?></option>
			<option value="fa-tumblr" <?php if($instance['social_icon3']=='fa-tumblr') echo 'selected'; ?>><?php _e('Tumblr','corpbiz'); ?></option>
			<option value="fa-vk" <?php if($instance['social_icon3']=='fa-vk') echo 'selected'; ?>><?php _e('Vk','corpbiz'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'social_url3' ); ?>"><?php _e( 'URL','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'social_url3' ); ?>" name="<?php echo $this->get_field_name( 'social_url3' ); ?>" type="text" value="<?php echo esc_attr( $instance['social_url3'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'social_icon4' ); ?>"><?php _e( 'Social Icon','corpbiz' ); ?></label> 
		<select id="<?php echo $this->get_field_id( 'social_icon4' ); ?>" name="<?php echo $this->get_field_name( 'social_icon4' ); ?>">
			<option value="fa-facebook" <?php if($instance['social_icon4']=='fa-facebook') echo 'selected'; ?>><?php _e('Facebook','corpbiz'); ?></option>
			<option value="fa-twitter" <?php if($instance['social_icon4']=='fa-twitter') echo 'selected'; ?>><?php _e('Twitter','corpbiz'); ?></option>
			<option value="fa-linkedin" <?php if($instance['social_icon4']=='fa-linkedin') echo 'selected'; ?>><?php _e('LinkedIn','corpbiz'); ?></option>
			<option value="fa-google-plus" <?php if($instance['social_icon4']=='fa-google-plus') echo 'selected'; ?>><?php _e('GooglePlus','corpbiz'); ?></option>
			<option value="fa-dribbble" <?php if($instance['social_icon4']=='fa-dribbble') echo 'selected'; ?>><?php _e('Dribbble','corpbiz'); ?></option>
			<option value="fa-dropbox" <?php if($instance['social_icon4']=='fa-dropbox') echo 'selected'; ?>><?php _e('Dropbox','corpbiz'); ?></option>
			<option value="fa-flickr" <?php if($instance['social_icon4']=='fa-flickr') echo 'selected'; ?>><?php _e('Flickr','corpbiz'); ?></option>
			<option value="fa-instagram" <?php if($instance['social_icon4']=='fa-instagram') echo 'selected'; ?>><?php _e('Instagram','corpbiz'); ?></option>
			<option value="fa-tumblr" <?php if($instance['social_icon4']=='fa-tumblr') echo 'selected'; ?>><?php _e('Tumblr','corpbiz'); ?></option>
			<option value="fa-vk" <?php if($instance['social_icon4']=='fa-vk') echo 'selected'; ?>><?php _e('Vk','corpbiz'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'social_url4' ); ?>"><?php _e( 'URL','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'social_url4' ); ?>" name="<?php echo $this->get_field_name( 'social_url4' ); ?>" type="text" value="<?php echo esc_attr( $instance['social_url4'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'social_icon5' ); ?>"><?php _e( 'Social Icon','corpbiz' ); ?></label> 
		<select id="<?php echo $this->get_field_id( 'social_icon5' ); ?>" name="<?php echo $this->get_field_name( 'social_icon5' ); ?>">
			<option value="fa-facebook" <?php if($instance['social_icon5']=='fa-facebook') echo 'selected'; ?>><?php _e('Facebook','corpbiz'); ?></option>
			<option value="fa-twitter" <?php if($instance['social_icon5']=='fa-twitter') echo 'selected'; ?>><?php _e('Twitter','corpbiz'); ?></option>
			<option value="fa-linkedin" <?php if($instance['social_icon5']=='fa-linkedin') echo 'selected'; ?>><?php _e('LinkedIn','corpbiz'); ?></option>
			<option value="fa-google-plus" <?php if($instance['social_icon5']=='fa-google-plus') echo 'selected'; ?>><?php _e('GooglePlus','corpbiz'); ?></option>
			<option value="fa-dribbble" <?php if($instance['social_icon5']=='fa-dribbble') echo 'selected'; ?>><?php _e('Dribbble','corpbiz'); ?></option>
			<option value="fa-dropbox" <?php if($instance['social_icon5']=='fa-dropbox') echo 'selected'; ?>><?php _e('Dropbox','corpbiz'); ?></option>
			<option value="fa-flickr" <?php if($instance['social_icon5']=='fa-flickr') echo 'selected'; ?>><?php _e('Flickr','corpbiz'); ?></option>
			<option value="fa-instagram" <?php if($instance['social_icon5']=='fa-instagram') echo 'selected'; ?>><?php _e('Instagram','corpbiz'); ?></option>
			<option value="fa-tumblr" <?php if($instance['social_icon5']=='fa-tumblr') echo 'selected'; ?>><?php _e('Tumblr','corpbiz'); ?></option>
			<option value="fa-vk" <?php if($instance['social_icon5']=='fa-vk') echo 'selected'; ?>><?php _e('Vk','corpbiz'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'social_url5' ); ?>"><?php _e( 'URL','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'social_url5' ); ?>" name="<?php echo $this->get_field_name( 'social_url5' ); ?>" type="text" value="<?php echo esc_attr( $instance['social_url5'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'social_icon6' ); ?>"><?php _e( 'Social Icon','corpbiz' ); ?></label> 
		<select id="<?php echo $this->get_field_id( 'social_icon6' ); ?>" name="<?php echo $this->get_field_name( 'social_icon6' ); ?>">
			<option value="fa-facebook" <?php if($instance['social_icon6']=='fa-facebook') echo 'selected'; ?>><?php _e('Facebook','corpbiz'); ?></option>
			<option value="fa-twitter" <?php if($instance['social_icon6']=='fa-twitter') echo 'selected'; ?>>
			<?php _e('Twitter','corpbiz'); ?></option>
			<option value="fa-linkedin" <?php if($instance['social_icon6']=='fa-linkedin') echo 'selected'; ?>><?php _e('LinkedIn','corpbiz'); ?></option>
			<option value="fa-google-plus" <?php if($instance['social_icon6']=='fa-google-plus') echo 'selected'; ?>><?php _e('GooglePlus','corpbiz'); ?></option>
			<option value="fa-dribbble" <?php if($instance['social_icon6']=='fa-dribbble') echo 'selected'; ?>><?php _e('Dribbble','corpbiz'); ?></option>
			<option value="fa-dropbox" <?php if($instance['social_icon6']=='fa-dropbox') echo 'selected'; ?>>
			<?php _e('Dropbox','corpbiz'); ?></option>
			<option value="fa-flickr" <?php if($instance['social_icon6']=='fa-flickr') echo 'selected'; ?>>
			<?php _e('Flickr','corpbiz'); ?></option>
			<option value="fa-instagram" <?php if($instance['social_icon6']=='fa-instagram') echo 'selected'; ?>><?php _e('Instagram','corpbiz'); ?></option>
			<option value="fa-tumblr" <?php if($instance['social_icon6']=='fa-tumblr') echo 'selected'; ?>>
			<?php _e('Tumblr','corpbiz'); ?></option>
			<option value="fa-vk" <?php if($instance['social_icon6']=='fa-vk') echo 'selected'; ?>><?php _e('Vk','corpbiz'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'social_url6' ); ?>"><?php _e( 'URL','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'social_url6' ); ?>" name="<?php echo $this->get_field_name( 'social_url6' ); ?>" type="text" value="<?php echo esc_attr( $instance['social_url6'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'social_icon7' ); ?>"><?php _e( 'Social Icon','corpbiz' ); ?></label> 
		<select id="<?php echo $this->get_field_id( 'social_icon7' ); ?>" name="<?php echo $this->get_field_name( 'social_icon7' ); ?>">
			<option value="fa-facebook" <?php if($instance['social_icon7']=='fa-facebook') echo 'selected'; ?>><?php _e('Facebook','corpbiz'); ?></option>
			<option value="fa-twitter" <?php if($instance['social_icon7']=='fa-twitter') echo 'selected'; ?>><?php _e('Twitter','corpbiz'); ?></option>
			<option value="fa-linkedin" <?php if($instance['social_icon7']=='fa-linkedin') echo 'selected'; ?>><?php _e('LinkedIn','corpbiz'); ?></option>
			<option value="fa-google-plus" <?php if($instance['social_icon7']=='fa-google-plus') echo 'selected'; ?>><?php _e('GooglePlus','corpbiz'); ?></option>
			<option value="fa-dribbble" <?php if($instance['social_icon7']=='fa-dribbble') echo 'selected'; ?>><?php _e('Dribbble','corpbiz'); ?></option>
			<option value="fa-dropbox" <?php if($instance['social_icon7']=='fa-dropbox') echo 'selected'; ?>><?php _e('Dropbox','corpbiz'); ?></option>
			<option value="fa-flickr" <?php if($instance['social_icon7']=='fa-flickr') echo 'selected'; ?>><?php _e('Flickr','corpbiz'); ?></option>
			<option value="fa-instagram" <?php if($instance['social_icon7']=='fa-instagram') echo 'selected'; ?>><?php _e('Instagram','corpbiz'); ?></option>
			<option value="fa-tumblr" <?php if($instance['social_icon7']=='fa-tumblr') echo 'selected'; ?>><?php _e('Tumblr','corpbiz'); ?></option>
			<option value="fa-vk" <?php if($instance['social_icon7']=='fa-vk') echo 'selected'; ?>><?php _e('Vk','corpbiz'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'social_url7' ); ?>"><?php _e( 'URL','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'social_url7' ); ?>" name="<?php echo $this->get_field_name( 'social_url7' ); ?>" type="text" value="<?php echo esc_attr( $instance['social_url7'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'social_icon8' ); ?>"><?php _e( 'Social Icon','corpbiz' ); ?></label> 
		<select id="<?php echo $this->get_field_id( 'social_icon8' ); ?>" name="<?php echo $this->get_field_name( 'social_icon8' ); ?>">
			<option value="fa-facebook" <?php if($instance['social_icon8']=='fa-facebook') echo 'selected'; ?>><?php _e('Facebook','corpbiz'); ?></option>
			<option value="fa-twitter" <?php if($instance['social_icon8']=='fa-twitter') echo 'selected'; ?>><?php _e('Twitter','corpbiz'); ?></option>
			<option value="fa-linkedin" <?php if($instance['social_icon8']=='fa-linkedin') echo 'selected'; ?>><?php _e('LinkedIn','corpbiz'); ?></option>
			<option value="fa-google-plus" <?php if($instance['social_icon8']=='fa-google-plus') echo 'selected'; ?>><?php _e('GooglePlus','corpbiz'); ?></option>
			<option value="fa-dribbble" <?php if($instance['social_icon8']=='fa-dribbble') echo 'selected'; ?>><?php _e('Dribbble','corpbiz'); ?></option>
			<option value="fa-dropbox" <?php if($instance['social_icon8']=='fa-dropbox') echo 'selected'; ?>><?php _e('Dropbox','corpbiz'); ?></option>
			<option value="fa-flickr" <?php if($instance['social_icon8']=='fa-flickr') echo 'selected'; ?>><?php _e('Flickr','corpbiz'); ?></option>
			<option value="fa-instagram" <?php if($instance['social_icon8']=='fa-instagram') echo 'selected'; ?>><?php _e('Instagram','corpbiz'); ?></option>
			<option value="fa-tumblr" <?php if($instance['social_icon8']=='fa-tumblr') echo 'selected'; ?>><?php _e('Tumblr','corpbiz'); ?></option>
			<option value="fa-vk" <?php if($instance['social_icon8']=='fa-vk') echo 'selected'; ?>><?php _e('Vk','corpbiz'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'social_url8' ); ?>"><?php _e( 'URL','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'social_url8' ); ?>" name="<?php echo $this->get_field_name( 'social_url8' ); ?>" type="text" value="<?php echo esc_attr( $instance['social_url8'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'social_icon9' ); ?>"><?php _e( 'Social Icon','corpbiz' ); ?></label> 
		<select id="<?php echo $this->get_field_id( 'social_icon9' ); ?>" name="<?php echo $this->get_field_name( 'social_icon9' ); ?>">
			<option value="fa-facebook" <?php if($instance['social_icon9']=='fa-facebook') echo 'selected'; ?>><?php _e('Facebook','corpbiz'); ?></option>
			<option value="fa-twitter" <?php if($instance['social_icon9']=='fa-twitter') echo 'selected'; ?>><?php _e('Twitter','corpbiz'); ?></option>
			<option value="fa-linkedin" <?php if($instance['social_icon9']=='fa-linkedin') echo 'selected'; ?>><?php _e('LinkedIn','corpbiz'); ?></option>
			<option value="fa-google-plus" <?php if($instance['social_icon9']=='fa-google-plus') echo 'selected'; ?>><?php _e('GooglePlus','corpbiz'); ?></option>
			<option value="fa-dribbble" <?php if($instance['social_icon9']=='fa-dribbble') echo 'selected'; ?>><?php _e('Dribbble','corpbiz'); ?></option>
			<option value="fa-dropbox" <?php if($instance['social_icon9']=='fa-dropbox') echo 'selected'; ?>><?php _e('Dropbox','corpbiz'); ?></option>
			<option value="fa-flickr" <?php if($instance['social_icon9']=='fa-flickr') echo 'selected'; ?>><?php _e('Flickr','corpbiz'); ?></option>
			<option value="fa-instagram" <?php if($instance['social_icon9']=='fa-instagram') echo 'selected'; ?>><?php _e('Instagram','corpbiz'); ?></option>
			<option value="fa-tumblr" <?php if($instance['social_icon9']=='fa-tumblr') echo 'selected'; ?>><?php _e('Tumblr','corpbiz'); ?></option>
			<option value="fa-vk" <?php if($instance['social_icon9']=='fa-vk') echo 'selected'; ?>><?php _e('Vk','corpbiz'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'social_url9' ); ?>"><?php _e( 'URL','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'social_url9' ); ?>" name="<?php echo $this->get_field_name( 'social_url9' ); ?>" type="text" value="<?php echo esc_attr( $instance['social_url9'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'social_icon10' ); ?>"><?php _e( 'Social Icon','corpbiz' ); ?></label> 
		<select id="<?php echo $this->get_field_id( 'social_icon10' ); ?>" name="<?php echo $this->get_field_name( 'social_icon10' ); ?>">
			<option value="fa-facebook" <?php if($instance['social_icon10']=='fa-facebook') echo 'selected'; ?>><?php _e('Facebook','corpbiz'); ?></option>
			<option value="fa-twitter" <?php if($instance['social_icon10']=='fa-twitter') echo 'selected'; ?>><?php _e('Twitter','corpbiz'); ?></option>
			<option value="fa-linkedin" <?php if($instance['social_icon10']=='fa-linkedin') echo 'selected'; ?>><?php _e('LinkedIn','corpbiz'); ?></option>
			<option value="fa-google-plus" <?php if($instance['social_icon10']=='fa-google-plus') echo 'selected'; ?>><?php _e('GooglePlus','corpbiz'); ?></option>
			<option value="fa-dribbble" <?php if($instance['social_icon10']=='fa-dribbble') echo 'selected'; ?>><?php _e('Dribbble','corpbiz'); ?></option>
			<option value="fa-dropbox" <?php if($instance['social_icon10']=='fa-dropbox') echo 'selected'; ?>><?php _e('Dropbox','corpbiz'); ?></option>
			<option value="fa-flickr" <?php if($instance['social_icon10']=='fa-flickr') echo 'selected'; ?>><?php _e('Flickr','corpbiz'); ?></option>
			<option value="fa-instagram" <?php if($instance['social_icon10']=='fa-instagram') echo 'selected'; ?>><?php _e('Instagram','corpbiz'); ?></option>
			<option value="fa-tumblr" <?php if($instance['social_icon10']=='fa-tumblr') echo 'selected'; ?>><?php _e('Tumblr','corpbiz'); ?></option>
			<option value="fa-vk" <?php if($instance['social_icon10']=='fa-vk') echo 'selected'; ?>><?php _e('Vk','corpbiz'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'social_url10' ); ?>"><?php _e( 'URL','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'social_url10' ); ?>" name="<?php echo $this->get_field_name( 'social_url10' ); ?>" type="text" value="<?php echo esc_attr( $instance['social_url10'] ); ?>" />
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'window_open' ); ?>"><?php _e( 'Open link in new tab','corpbiz' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'window_open' ); ?>" name="<?php echo $this->get_field_name( 'window_open' ); ?>" type="checkbox" <?php if($instance['window_open']==true) echo 'checked'; ?> />
	</p>
	
	
	<p>
	<?php _e( 'Find Font Awesome icon like : fa-facebook','corpbiz' ); ?> <a href="https://fortawesome.github.io/Font-Awesome/icons/" target="_blank"><?php _e( 'click here','corpbiz' ); ?><a>
	</p>
	
	<?php
    }
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : '';
		$instance['window_open'] = ( ! empty( $new_instance['window_open'] ) ) ? $new_instance['window_open'] : '';
		
		$instance['social_icon1'] = ( ! empty( $new_instance['social_icon1'] ) ) ? $new_instance['social_icon1'] : '';
		$instance['social_url1'] = ( ! empty( $new_instance['social_url1'] ) ) ? $new_instance['social_url1'] : '';
		
		$instance['social_icon2'] = ( ! empty( $new_instance['social_icon2'] ) ) ? $new_instance['social_icon2'] : '';
		$instance['social_url2'] = ( ! empty( $new_instance['social_url2'] ) ) ? $new_instance['social_url2'] : '';
		
		$instance['social_icon3'] = ( ! empty( $new_instance['social_icon3'] ) ) ? $new_instance['social_icon3'] : '';
		$instance['social_url3'] = ( ! empty( $new_instance['social_url3'] ) ) ? $new_instance['social_url3'] : '';
		
		$instance['social_icon4'] = ( ! empty( $new_instance['social_icon4'] ) ) ? $new_instance['social_icon4'] : '';
		$instance['social_url4'] = ( ! empty( $new_instance['social_url4'] ) ) ? $new_instance['social_url4'] : '';
		
		$instance['social_icon5'] = ( ! empty( $new_instance['social_icon5'] ) ) ? $new_instance['social_icon5'] : '';
		$instance['social_url5'] = ( ! empty( $new_instance['social_url5'] ) ) ? $new_instance['social_url5'] : '';
		
		$instance['social_icon6'] = ( ! empty( $new_instance['social_icon6'] ) ) ? $new_instance['social_icon6'] : '';
		$instance['social_url6'] = ( ! empty( $new_instance['social_url6'] ) ) ? $new_instance['social_url6'] : '';
		
		$instance['social_icon7'] = ( ! empty( $new_instance['social_icon7'] ) ) ? $new_instance['social_icon7'] : '';
		$instance['social_url7'] = ( ! empty( $new_instance['social_url7'] ) ) ? $new_instance['social_url7'] : '';
		
		$instance['social_icon8'] = ( ! empty( $new_instance['social_icon8'] ) ) ? $new_instance['social_icon8'] : '';
		$instance['social_url8'] = ( ! empty( $new_instance['social_url8'] ) ) ? $new_instance['social_url8'] : '';
		
		$instance['social_icon9'] = ( ! empty( $new_instance['social_icon9'] ) ) ? $new_instance['social_icon9'] : '';
		$instance['social_url9'] = ( ! empty( $new_instance['social_url9'] ) ) ? $new_instance['social_url9'] : '';
		
		$instance['social_icon10'] = ( ! empty( $new_instance['social_icon10'] ) ) ? $new_instance['social_icon10'] : '';
		$instance['social_url10'] = ( ! empty( $new_instance['social_url10'] ) ) ? $new_instance['social_url10'] : '';
		
		return $instance;
	}
}