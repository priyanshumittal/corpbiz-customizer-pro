<?php
// Template Name: Contact-Us
get_header(); 
get_template_part('index', 'banner');
if(!is_home()) { echo '</div>'; }
$corpbiz_options=theme_data_setup(); 
$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), $corpbiz_options ); ?>
<div class="container">	
	<div class="row contact_section">
	<?php if( $post->post_content != "" ) { ?>
	<div class="col-md-12">
		<?php the_post(); the_content();?>
	</div>
	<?php } ?>
		<?php if($current_options['send_usmessage'] !='') { ?>
		<div class="col-md-9">
			<div class="row">
			 <div class="col-md-12">
			    <h2 class="contact_form_title"><?php echo $current_options['send_usmessage']; ?></h2>
				</div>
				<?php } ?>
			</div>
			<form  method="POST" id="contact_form" name="contact_form" action="#">
				<div class="row">
					<div class="form-group">
						<div class="col-md-4">
							<label><?php _e('Name','corpbiz'); ?>*</label>
							<input type="text" name="user_name" id="user_name" class="contact_input_control">
							<span  style="display:none; color:red" id="contact_user_name_error"><?php _e('Name','corpbiz'); ?> </span>
						</div>
						<div class="col-md-4">
							<label><?php _e('Email','corpbiz'); ?>*</label>
							<input type="text" name="user_email" id="user_email" class="contact_input_control">
							<span  style="display:none; color:red" id="contact_user_email_error"><?php _e('Email','corpbiz'); ?> </span>
						</div>
						<div class="col-md-4">
							<label><?php _e('Subject','corpbiz'); ?></label>
							<input type="text" name="user_subject" id="user_subject" class="contact_input_control">
							<span  style="display:none; color:red" id="contact_user_subject_error"><?php _e('Subject','corpbiz'); ?> </span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-md-12">
							<label><?php _e('Message','corpbiz'); ?></label>
							<textarea name="user_massage" id="user_massage" class="contact_textarea_control" rows="7"></textarea>
							<span  style="display:none; color:red" id="contact_user_massage_error"><?php _e('Message','corpbiz'); ?> </span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<button class="btn_common" type="submit" name="contact_submit" ><?php _e('Send Message','corpbiz'); ?></button>
					</div>
				</div>
			</form>
			<div id="mailsentbox" style="display:none">
				<div class="alert alert-success" >
					<strong><?php _e('Thank you','corpbiz');?></strong> <?php _e('Your information has been sent.','corpbiz');?>
				</div>
			</div>
			
		</div>
		<?php
		if(isset($_POST['contact_submit']))
		{ 	
			if($_POST['user_name'] ==''){					
				echo "<script>jQuery('#contact_user_name_error').show();</script>";
			} 
			else 
			if($_POST['user_email']=='') {	
				echo "<script>jQuery('#contact_user_email_error').show();</script>";
			}
			else
			if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i",$_POST['user_email'])) {	
				echo "<script>jQuery('#contact_user_email_error').show();</script>";
			} 
			else
			if($_POST['user_subject'] == ''){	
				echo "<script>jQuery('#contact_user_subject_error').show();</script>";
			}
			else
			if($_POST['user_massage']=='')
			{
				echo "<script>jQuery('#contact_user_massage_error').show();</script>";
			}
			else
			{	
				$to = get_option('admin_email');
				$subject = trim($_POST['user_name']) . $_POST['user_subject'] . get_option("blogname");
				$massage = stripslashes(trim($_POST['user_massage']))."Message sent from:: ".trim($_POST['user_email']);
				$headers = "From: ".trim($_POST['user_name'])." <".trim($_POST['user_email']).">\r\nReply-To:".trim($_POST['user_email']);
				$maildata =wp_mail($to, $subject, $massage, $headers); 
				echo "<script>jQuery('#myformdata').hide();</script>";
				echo "<script>jQuery('#mailsentbox').show();</script>";							
			}
		}
		
		?>
		
		<div class="col-md-3 contact_detail">
			<div class="row">
				<div class="col-md-12">
				<?php if($current_options['contact_info_enabled'] == true) { ?>
				
				<h2><?php echo $current_options['contact_info_title']; ?></h2>
					<p><?php echo $current_options['contect_info_description'];?></p>
				<?php } ?>	
					<address>
						<?php if($current_options['contact_address']!='') { echo $current_options['contact_address']; } ?>
						<br> <?php if($current_options['contact_address_two']!='') { echo $current_options['contact_address_two']; } ?>
						<br><?php if($current_options['contact_email']!='') { ?>
						<a href="mailto:<?php  echo $current_options['contact_email']; ?>"> <?php echo $current_options['contact_email']; ?></a>
						<?php } ?>
						<?php if($current_options['contact_phone_number']!='') { ?>
						<br>+<?php echo $current_options['contact_phone_number']; ?>
						<?php } ?>
						<br>
					</address>
				</div>
			</div>
		</div>
    </div>
</div>
<?php 
$mapsrc = $current_options['contact_google_map_url'];

if($current_options['contact_google_map_enabled']==true) {
	
	if(!empty($mapsrc)) 
	{
		$mapsrc=$mapsrc.'&amp;output=embed';
 ?>
<div class="container">
	<div class="row">
	<div class="col-md-12">
		<div class="google_map">			
			<iframe style="border:none;" width="100%" height="400"  src="<?php echo $mapsrc; ?>" ></iframe>	
		</div>
		</div>
	</div>
</div>
<?php } 
}
if( $current_options['contact_callout_disable'] == false ):
	get_template_part('index','call-out-area');
endif;
?>
<?php get_footer(); ?>