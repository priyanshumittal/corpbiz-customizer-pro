<?php 
//Template Name: ABOUT US
$corpbiz_options=theme_data_setup(); 
$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), $corpbiz_options ); ?>
<!-- Header Section -->
<!--Logo & Menu Section-->
<?php get_header(); ?>
<!--/Logo & Menu Section--> 
<?php get_template_part('index','banner');?>
<!-- /Header Section -->
<?php if(!is_home()) { echo '</div>'; }?>
<!-- /Page Section -->
<!--About Buy Now Section-->
<?php if( $post->post_content != "" ) { ?>
<div class="aboutus_buynow_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php 
				the_post(); 
				the_content();?>
			</div>	
		</div>						
	</div>	
</div>
<?php } ?>
<!--/About Buy Now Section-->

<!--Homepage Service Section-->
<?php 
$corpbiz_options=theme_data_setup(); 
$current_options = wp_parse_args(  get_option( 'corpbiz_options', array() ), $corpbiz_options ); 
if($current_options['service_section_about_enable'] == false) { ?>
<div class="service_section">
	<div class="container">
			<div class="row">
				<div class="section_title">
					<?php if($current_options['home_service_title'] !="") { ?>
					<h1 class="widget-title"><?php echo $current_options['home_service_title']; ?></h1>
					<?php } ?>
					<?php if($current_options['home_service_description'] !="") { ?>
					<p><?php echo $current_options['home_service_description']; ?> </p>
					<?php } ?>
				</div>	
			</div>
			<div class="row">
			<?php
				$i=1;
				$count_posts = wp_count_posts( 'corpbiz_service')->publish;			
				$args = array( 'post_type' => 'corpbiz_service','posts_per_page' =>$count_posts); 	
				$service = new WP_Query( $args );
				if( $service->have_posts() )
				{ while ( $service->have_posts() ) : $service->the_post(); ?>
				<div class="col-md-3 col-sm-6">			
						<div class="service_area">
						<?php if(get_post_meta( get_the_ID(),'meta_service_link', true )) 
						{ $meta_service_link=get_post_meta( get_the_ID(),'meta_service_link', true ); }
						else
						{ $meta_service_link = ""; }						
						 if(has_post_thumbnail()){  ?>
							<div class="service_box">
								<?php if($meta_service_link){
										$defalt_arg =array('class' => "img-responsive"); ?> 
										<a href="<?php echo $meta_service_link;  ?>" <?php if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo 'target="_blank"'; } ?>> <?php the_post_thumbnail('', $defalt_arg); ?> </a>
								<?php } else {
										$defalt_arg =array('class' => "img-responsive"); 
										the_post_thumbnail('', $defalt_arg);
								} ?>
							</div>
							<?php } else {
							if(get_post_meta( get_the_ID(),'service_icon_image', true )) {?>
							<div class="service_box">
							<?php if($meta_service_link){ ?>
							<a href="<?php echo $meta_service_link;  ?>" <?php if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo 'target="_blank"'; } ?>> <i class="fa <?php echo get_post_meta( get_the_ID(),'service_icon_image', true ); ?>"></i> </a>
							<?php } else { ?>
							<i class="fa <?php echo get_post_meta( get_the_ID(),'service_icon_image', true ); ?>"></i>
							<?php } ?>
							</div>
						<?php }
						} ?>
						<?php if($meta_service_link){ ?>
							<h2><a href="<?php echo $meta_service_link;  ?>" <?php if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo 'target="_blank"'; } ?>><?php the_title(); ?></a></h2>
						<?php } else { ?>
							<h2><?php the_title(); ?></h2>
						<?php } ?>
					<p><?php echo get_post_meta( get_the_ID(), 'service_description_text', true ); ?></p>
					</div>
				</div>
			<?php if($i%4==0)
				{	echo "<div class='clearfix'></div>"; 	}
				$i++; endwhile;
			} else { ?>
			<div class="col-md-3 col-sm-6">
				<div class="service_area">
					<div class="service_box">
						<i class="fa fa-mobile service_icon_green"></i>
					</div>
					<h2><?php _e('Responsive design','corpbiz'); ?></h2>
					<p><?php echo 'Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget ...'; ?></p>
				</div>
			</div>
			
			<div class="col-md-3 col-sm-6">
				<div class="service_area">
					<div class="service_box">
						<i class="fa fa-rocket service_icon_red"></i>
					</div>
					<h2><?php _e('Power full admin','corpbiz'); ?></h2>
					<p><?php echo 'Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget ...'; ?></p>
				</div>
			</div>
			
			<div class="col-md-3 col-sm-6">
				<div class="service_area">
					<div class="service_box">
						<i class="fa fa-thumbs-o-up service_icon_blue"></i>
					</div>
					<h2><?php _e('Great support','corpbiz'); ?></h2>
					<p><?php echo 'Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget'; ?></p>
				</div>
			</div>
			
			<div class="col-md-3 col-sm-6">
				<div class="service_area">
					<div class="service_box">
						<i class="fa fa-laptop service_icon_orange"></i>
					</div>
					<h2><?php _e('Great support','corpbiz'); ?></h2>
					<p><?php echo 'Lorem ipsum dolor sit amet, consect adipiscing elit.ivamus eget'; ?></p>
				</div>
			</div>
			<?php } ?>		
		</div>	
	</div>
</div>
<!--/Homepage Service Section-->
<?php } ?>
<!--About Team Section-->
<?php 
if($current_options['team_section_enable'] == false) { ?>
<div class="about_team_section">
	<div class="container">
		<?php if($current_options['team_title']!='') { ?>
		<div class="row">
			<div class="col-md-12"><h1 class="about_team_title"><?php echo $current_options['team_title']; ?></h1></div>
		</div>	
		<?php } ?>
		<div class="row">
		<?php
			$j=1;
			$count_posts = wp_count_posts( 'corpbiz_team')->publish;
			$arg = array( 'post_type' => 'corpbiz_team','posts_per_page' =>$count_posts);
			$team = new WP_Query( $arg ); 
			if($team->have_posts())
			{	while ( $team->have_posts() ) : $team->the_post();	?>		
			<div class="col-md-3 col-sm-6">
				<div class="about_team_showcase">
				<?php $defalt_arg =array('class' => "img-responsive");
				if(has_post_thumbnail()): ?>
					<?php the_post_thumbnail('', $defalt_arg); ?>
				<?php endif; ?>					
					<div class="caption">
						<h3><?php the_title(); ?></h3>
						<?php if(get_post_meta( get_the_ID(), 'description_meta_save', true ) != '' ) { ?>
						<p><?php echo get_post_meta( get_the_ID(), 'description_meta_save', true ) ; ?></p>
						<?php }
						$fb_meta_save = get_post_meta( get_the_ID(), 'fb_meta_save', true );
						$fb_meta_save_chkbx = get_post_meta( get_the_ID(), 'fb_meta_save_chkbx', true );
						$twt_meta_save = get_post_meta( get_the_ID(), 'twt_meta_save', true );
						$twt_meta_save_chkbx = get_post_meta( get_the_ID(), 'twt_meta_save_chkbx', true );
						$flickr_meta_save = get_post_meta( get_the_ID(), 'flickr_meta_save', true );
						$flickr_meta_save_chkbx =  get_post_meta( get_the_ID(), 'flickr_meta_save_chkbx', true );
						$google_meta_save = get_post_meta( get_the_ID(), 'google_meta_save', true );
						$google_meta_save_chkbx =get_post_meta( get_the_ID(), 'google_meta_save_chkbx', true );	
						?>
						<ul class="about_team_social">
						<?php if($fb_meta_save): 
						if($fb_meta_save_chkbx)
						{	$target ="_blank";  } else { $target ="_self";  } ?>	
						<li class="facebook"><a target="<?php echo $target; ?>" href="<?php if($fb_meta_save){ echo esc_html($fb_meta_save); } ?>"><i class="fa fa-facebook-square"></i></a></li>
						<?php endif; ?>
						<?php if($twt_meta_save): 
						if($twt_meta_save_chkbx)
						{	$target ="_blank";  } else { $target ="_self";  } ?>	
							<li class="twitter"><a target="<?php echo $target; ?>" href="<?php if($twt_meta_save){ echo esc_html($twt_meta_save); } ?>"><i class="fa fa-twitter-square"></i></a></li>
						<?php endif; ?>
						<?php if($google_meta_save): 
						if($google_meta_save_chkbx)
						{	$target ="_blank";  } else { $target ="_self";  } ?>	
						<li class="googleplus"><a target="<?php echo $target; ?>" href="<?php if($google_meta_save){ echo esc_html($google_meta_save); } ?>"><i class="fa fa-google-plus-square"></i></a></li>
						<?php endif; ?>
						<?php if($flickr_meta_save): 
						if($flickr_meta_save_chkbx)
						{	$target ="_blank";  } else { $target ="_self";  } ?>	
						<li class="flickr"><a target="<?php echo $target; ?>" href="<?php if($flickr_meta_save){ echo esc_html($flickr_meta_save); } ?>"><i class="fa fa-flickr"></i></a></li>
						<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
			<?php if($j%4==0)
			{	echo "<div class='clearfix'></div>"; 	}
			$j++; endwhile ;
			}
			else
			{
			for($dp=1; $dp<=4; $dp++) { ?>
			<div class="col-md-3 col-sm-6">
				<div class="about_team_showcase">
				<img src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/client<?php echo $dp ;?>.jpg" alt="Corpo" class="img-responsive">
					<div class="caption">
						<h3><?php echo 'Bradley Grosh'; ?></h3>
						<p><?php echo 'Malesuada a viverra ac, pellentesque vitae nunc. Aenean ac leo eget nunc fringilla.'; ?></p>
						<ul class="about_team_social">
						   <li class="facebook"><a href="#"><i class="fa fa-facebook-square"></i></a></li>
						   <li class="twitter"><a href="#"><i class="fa fa-twitter-square"></i></a></li>
						   <li class="googleplus"><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
						   <li class="flickr"><a href="#"><i class="fa fa-flickr"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<?php }
			} ?>
		</div>
	</div>
</div>
<!--/About Team Section-->
<?php }
if($current_options['client_section_enable'] == false) {
get_template_part('index','testimonial');
}
if($current_options['footer_callout_section_enable'] == false) {
get_template_part('index', 'call-out-area');
}
get_footer();
?>