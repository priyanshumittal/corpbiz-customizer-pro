/*
 * jQuery FlexSlider v2.2.0
 * Copyright 2012 WooThemes
 * Created by ----Shahid
 */
 
jQuery(window).load(function(){
		  jQuery('.flexslider').flexslider({	
			animation: "slide",
			animationSpeed: 2000,
			direction: "slide",
			
			directionNav: true, 
			//prevText: "Previous",          
			//nextText: "Next",
			controlNav: true,			
			
			slideshowSpeed: 3000,
			pauseOnHover: true, 
			slideshow: true,
			start: function(slider){
			  jQuery('body').removeClass('loading');
			}			
		  });
		  
		  
		});